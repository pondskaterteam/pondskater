import smbus
import time

class rpirc:

	address = 0x04
	
	def __init__(self):
		self.bus = smbus.SMBus(1)
		self.StopDrive()
		time.sleep(0.1)
		self.SetServo(90)

	def SetDrive(self, speed):
		self.bus.write_byte(self.address, 195)
		time.sleep(0.01)
		self.bus.write_byte(self.address, 45+speed)

	def StopDrive(self):
		self.SetDrive(0)

	def SetServo(self, angle):
		self.bus.write_byte(self.address, 196)
		time.sleep(0.01)
		self.bus.write_byte(self.address, angle)

	def GetVoltage(self):
		var = self.bus.read_byte_data(self.address, 194)
		# 59 -> 12.4
		return var*0.21
		
	def GetSwitch(self):
		var = self.bus.read_byte_data(self.address, 191)
		return var
		
