import time
import socket
import os

#convert string to hex
def toHex(s):
    lst = []
    for ch in s:
        hv = hex(ord(ch)).replace('0x', '')
        if len(hv) == 1:
            hv = '0'+hv
        lst.append(hv)
    
    return reduce(lambda x,y:x+y, lst)

#HOST = '192.168.0.20'
HOST = 'localhost'	 
PORT_IN = 2000
PORT_OUT = 2001					 
FILE_PREFIX = "/var/log/raw_all"
#FILE_PREFIX = "/home/user/raw_all"
#FILE_PREFIX = "raw_all"

gUBXFile = open(FILE_PREFIX + str(toHex(os.urandom(10)))+".ubx", "wb")

s_in = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s_in.connect((HOST, PORT_IN))

s_out_srv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s_out_srv.bind(('localhost', PORT_OUT))
s_out_srv.listen(1)
s_out_srv.settimeout(0.1)

s_out = None

while (1):
	data = s_in.recv(4096)
	gUBXFile.write(data)
	
	if s_out is not None:
		try:
			s_out.sendall(data)
		except socket.error as msg:
			s_out.close()
			s_out = None
	else:	
		try:
			(s_out, address) = s_out_srv.accept()
		except socket.timeout as msg:
			s_out = None
