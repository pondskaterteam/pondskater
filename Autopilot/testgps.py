import ubx
import socket
import time
import base64
import os

gUTCTime = ()

def gps_callback(ty, *args):
	global gUTCTime
	print ty
	if ty == 'NAV-TIMEUTC':
		gUTCTime = (int(args[0][0]["Year"]),
			int(args[0][0]["Month"]),
			int(args[0][0]["Day"]),
			int(args[0][0]["Hour"]),
			int(args[0][0]["Min"]),
			int(args[0][0]["Sec"]),
			float(args[0][0]["Nano"])/1000000000)
			
		date_str = "{:0>4d}-{:0>2d}-{:0>2d} {:0>2d}:{:0>2d}:{:0>8.5f}".format(int(args[0][0]["Year"]),
			int(args[0][0]["Month"]),
			int(args[0][0]["Day"]),
			int(args[0][0]["Hour"]),
			int(args[0][0]["Min"]),
			int(args[0][0]["Sec"])+float(args[0][0]["Nano"])/1000000000,
			)
		print('date -u -set="%s"' % date_str)
#		os.system('date -u -set="%s"' % date_str)

def gps_process(sock, parser):
	data = sock.recv(4096)
	parser.parse(data)

print "Starting GPS..."

t = ubx.Parser(gps_callback, device=False)

HOST = 'localhost'	 
#HOST = '192.168.0.20' 
#PORT = 2000				 
PORT = 2001				 

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

gps_process(s, t)

while (1):
	gps_process(s, t)
#	print gUTCTime
	time.sleep(0.3)

