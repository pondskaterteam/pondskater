import time
import compass
import ubx
import socket
import rpirc
import math

gLatitude = 0
gLongitude = 0
gAccuracy = 0
gDateStr = ""

Kp = 1 # Proportional
Ki = 0 # Integral
Kd = 0 # Differential

Ktime = 0.1 # Cycle length 

FN = str(time.time())+ "_"+str(Kp)

print "File name: ", FN

gUBXFile = open("raw" + FN+".ubx", "w")

def gps_callback(ty, *args):
	global gLatitude, gLongitude, gAccuracy, gDateStr
	if ty == 'NAV-POSLLH':
		gLatitude = float(args[0][0]["LAT"])/10000000
		gLongitude = float(args[0][0]["LON"])/10000000
		gAccuracy = float(args[0][0]["Hacc"])/1000
		
	if ty == 'NAV-TIMEUTC':
		gDateStr = "{:0>4d}-{:0>2d}-{:0>2d} {:0>2d}:{:0>2d}:{:0>8.5f}".format(int(args[0][0]["Year"]),
			int(args[0][0]["Month"]),
			int(args[0][0]["Day"]),
			int(args[0][0]["Hour"]),
			int(args[0][0]["Min"]),
			int(args[0][0]["Sec"])+float(args[0][0]["Nano"])/1000000000,
			)

def gps_process(sock, parser):
	global gUBXFile
	data = sock.recv(1024)
	print "data length: ", len(data)
	gUBXFile.write(data)
	parser.parse(data)

print "Starting compass..."

oCompass = compass.compass()

# Brest, Belarus
# http://magnetic-declination.com/Belarus/Brest/317075.html
# http://www.ngdc.noaa.gov/geomag-web/#declination
oCompass.setDeclination(5, 56)

print "Starting RPI_RC..."

oRPIRC = rpirc.rpirc()

print "Starting GPS..."

t = ubx.Parser(gps_callback, device=False)

HOST = 'localhost'	 
PORT = 2001				 
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

gps_process(s, t)

while oRPIRC.GetSwitch() == 0:
	Voltage = oRPIRC.GetVoltage()
	gps_process(s, t)
	alpha = oCompass.getBearing()
	print ("Wait command. Bearing: %s. Voltage: %s" % (alpha, Voltage))
	time.sleep(0.2)

FN = FN + "_" + str(alpha)

gBearingFile = open("raw" + FN+".txt", "w")
	
#oRPIRC.SetDrive(46)
time.sleep(0.2)

IntegrSumm = 0
PrevDelta = 0

while (1):

	BegTime = time.time()

	beta = oCompass.getBearing()


	
	(millisec, sec) = math.modf(BegTime)
	
	millisec = int(millisec*1000)
	
	delta = beta - alpha
	if delta > 180:
		delta = 360 - delta
	if delta < -180:
		delta = 360 + delta
	
	IntegrSumm = IntegrSumm + Ki * delta
	gamma = Kp*delta + IntegrSumm + Kd * (delta - PrevDelta)
	PrevDelta = delta
	
	gamma = gamma + 90
	if gamma < 0:
		gamma = 0
	if gamma > 180:
		gamma = 180
		
	gamma = int(gamma)
	
	oRPIRC.SetServo(gamma)

	print >> gBearingFile, 	time.strftime("%Y/%m/%d %H:%M:%S", time.gmtime(BegTime))+"."+str(millisec), " ", beta
		
	gps_process(s, t)

	Voltage = oRPIRC.GetVoltage()
	
	print ("Run command. Bearing: %s. Voltage: %s" % (beta, Voltage))
#	print ("Run command. Bearing: %s" % (beta))

# Sleep remaining time	
	time.sleep( Ktime - (time.time() - BegTime) )

	
