import ubx
import socket
import time
import base64

gUTCTime = ()

def gps_callback(ty, *args):
	global gUTCTime
	print ty
	if ty == 'NAV-TIMEUTC':
		gUTCTime = (int(args[0][0]["Year"]),
			int(args[0][0]["Month"]),
			int(args[0][0]["Day"]),
			int(args[0][0]["Hour"]),
			int(args[0][0]["Min"]),
			int(args[0][0]["Sec"]),
			float(args[0][0]["Nano"])/1000000000)

def gps_process(sock, parser):
	data = sock.recv(4096)
	parser.parse(data)

print "Starting GPS..."

t = ubx.Parser(gps_callback, device=False)

HOST = 'localhost'	 
PORT = 2000				 

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

gps_process(s, t)

s.send(("\xff" * 8) + "\xb5\x62\x09\x01\x10\x00\xc8\x16\x00\x00\x00\x00\x00\x00\x97\x69\x21\x00\x00\x00\x02\x10\x2b\x22")
time.sleep(0.5)

gps_process(s, t)

s.send(("\xff" * 8) + "\xb5\x62\x09\x01\x10\x00\x0c\x19\x00\x00\x00\x00\x00\x00\x83\x69\x21\x00\x00\x00\x02\x11\x5f\xf0")

time.sleep(0.5)

gps_process(s, t)

s.send(("\xff" * 8) + "\xB5\x62\x06\x01\x08\x00\x02\x10\x00\x01\x00\x00\x00\x00\x22\x28")
time.sleep(0.5)

gps_process(s, t)

s.send(("\xff" * 8) + "\xB5\x62\x06\x01\x08\x00\x02\x11\x00\x01\x00\x00\x00\x00\x23\x2F")
time.sleep(0.5)


while (1):
	gps_process(s, t)
	print gUTCTime
	time.sleep(0.3)

