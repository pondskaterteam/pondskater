import smbus
import time

# for RPI version 1, use "bus = smbus.SMBus(0)"
bus = smbus.SMBus(1)

# This is the address we setup in the Arduino Program
address = 0x04

print "=== Drive test ==="

time.sleep(2)

print "Stop drive"

bus.write_byte(address, 195)
time.sleep(0.1)
bus.write_byte(address, 45)
time.sleep(1)

print "Start drive"
bus.write_byte(address, 195)
time.sleep(0.1)
bus.write_byte(address, 60)
time.sleep(2)

print "Stop drive"
bus.write_byte(address, 195)
time.sleep(0.1)
bus.write_byte(address, 45)
time.sleep(1)

var = bus.read_byte_data(address, 190)
print "Signal: ", var

time.sleep(0.1)

var = bus.read_byte_data(address, 191)
print "Switch: ", var

time.sleep(0.1)

var = bus.read_byte_data(address, 192)
print "Drive stick: ", var

time.sleep(0.1)

var = bus.read_byte_data(address, 193)
print "Servo stick: ", var

time.sleep(0.1)

var = bus.read_byte_data(address, 194)
# 59 -> 12.4
print "Voltage: ", var*0.21

time.sleep(0.1)

print "=== Servo test ==="
bus.write_byte(address, 196)
time.sleep(0.1)
bus.write_byte(address, 45)

time.sleep(1)

bus.write_byte(address, 196)
time.sleep(0.1)
bus.write_byte(address, 135)

time.sleep(1)

bus.write_byte(address, 196)
time.sleep(0.1)
bus.write_byte(address, 90)

time.sleep(1)

