#pragma once

#include "std.h"

struct DateTime
{
	DateTime( ){}
	DateTime( QDate date, QTime time );

	QDate date;
	QTime time;
};

class PixmapFileCache
{
	struct TileProperties
	{
		TileProperties();
		TileProperties( long long, QDate, QTime );

		long long size;
		QDate date;
		QTime time;
	};
	
public:
	PixmapFileCache();
	~PixmapFileCache( );

	void setCacheDirPath( QString path );
	void setCacheLimit( int size ); // mbytes
	void setRamLimit( int size ); // mbytes

	long long cacheLimit();
	
	bool insert( const QString &key, QPixmap* pixmap );
	bool remove( const QString &key );
	
	/*bool find( const QString &key );*/
	QPixmap* load( const QString &key );
	
	void clear();

private:
	void loadCacheProperties();
	void saveCacheProperties();
	void freeRamCache();
	void freeFileCache();

private:
	QString cachePath;
	long long fileLimit, fileSize;
	long long ramLimit, ramSize;
	bool correctDir;

	QMap< QString, QPixmap* > pixmapTileMap;
	QMap< QString, TileProperties > tilePropertiesMap;
// 	QMultiMap< QString, QString > timeRamCacheMap;
// 	QMultiMap< QString, QString > timeFileCacheMap;
};
