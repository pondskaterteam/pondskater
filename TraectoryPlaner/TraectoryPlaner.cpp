#include "TraectoryPlaner.h"

TraectoryPlaner::TraectoryPlaner( QWidget* parent ) : QMainWindow( parent )
{
	mapLayer = new MapLayer();
	pathModelLayer = new PathModelLayer();
	mapStateLayer = new MapStateLayer;

	mapLayer->setImageManager( &imageManager );

	ui.setupUi( this );

	QApplication::setOrganizationName( "BSTU Robotics" );
	QApplication::setApplicationName( "Traectory Planer" );

	if( !QFile( "settings.ini" ).exists() ) // ��������� �����������
	{
		QFile( ":TraectoryPlanner/defaults.ini" ).copy( QDir().toNativeSeparators( QDir().absolutePath() + QDir().separator() + "settings.ini" ) );
		QFile( "settings.ini" ).setPermissions( QFile::Permission::WriteOwner | QFile::Permission::ReadOwner | QFile::Permission::ExeOwner );
	}
		
	trajectoryStyle.updateColors();
	barrierStyle.updateColors();

	readSettings( );

	ui.mapControl->addLayer( mapLayer );
	ui.mapControl->addLayer( pathModelLayer );
	ui.mapControl->addLayer( mapStateLayer );

	ui.addCheckPointButton->setShortcut( QKeySequence( Qt::CTRL + Qt::Key_A ) );
	ui.insertCheckPointButton->setShortcut( QKeySequence( Qt::CTRL + Qt::Key_S ) );
	ui.deleteCheckPointButton->setShortcut( QKeySequence( Qt::CTRL + Qt::Key_D ) );
	ui.roolUpButton->setShortcut( QKeySequence( Qt::CTRL + Qt::Key_E ) );

	for( uint i = 0; i < ui.editWidget->getPathModelCount(); i++ )
		pathModelLayer->addPathModel( ui.editWidget->getPathModel( i ), i ? ( PathStyle* ) &trajectoryStyle : ( PathStyle* ) &barrierStyle );//��������

	QObject::connect( ui.toolBarWidget, SIGNAL( toolBarEvent( ToolBarEvent ) ), pathModelLayer, SLOT( toolBarEvent( ToolBarEvent ) ) );
	ui.tabWidget->currentChanged( ui.tabWidget->currentIndex() );

	allocator = new CheckpointItemAllocator();

	ui.tableBarrierPolygon->setObjectModel( ui.editWidget->getPathModel( 0 ) );
	ui.tableBarrierPolygon->setAllocator( allocator );
	ui.tablePath->setObjectModel( ui.editWidget->getPathModel( 1 ) );
	//ui.tablePath->setItemHeight( 40 );
	ui.tablePath->setAllocator( allocator );

	QObject::connect( ui.editWidget->getPathModel( 0 ), SIGNAL( changed( int ) ), ui.tableBarrierPolygon, SLOT( changed( int ) ) );
	QObject::connect( ui.editWidget->getPathModel( 0 ), SIGNAL( inserted( int, int ) ), ui.tableBarrierPolygon, SLOT( inserted( int, int ) ) );
	QObject::connect( ui.editWidget->getPathModel( 0 ), SIGNAL( moved( int, int, int ) ), ui.tableBarrierPolygon, SLOT( moved( int, int, int ) ) );
	QObject::connect( ui.editWidget->getPathModel( 0 ), SIGNAL( removed( int, int ) ), ui.tableBarrierPolygon, SLOT( removed( int, int ) ) );	

	QObject::connect( ui.editWidget->getPathModel( 1 ), SIGNAL( changed( int ) ), ui.tablePath, SLOT( changed( int ) ) );
	QObject::connect( ui.editWidget->getPathModel( 1 ), SIGNAL( inserted( int, int ) ), ui.tablePath, SLOT( inserted( int, int ) ) );
	QObject::connect( ui.editWidget->getPathModel( 1 ), SIGNAL( moved( int, int, int ) ), ui.tablePath, SLOT( moved( int, int, int ) ) );
	QObject::connect( ui.editWidget->getPathModel( 1 ), SIGNAL( removed( int, int ) ), ui.tablePath, SLOT( removed( int, int ) ) );

	QObject::connect( ui.tableBarrierPolygon, SIGNAL( focusChanged( int ) ), this, SLOT( focusChanged( int ) ) );
	QObject::connect( ui.tablePath, SIGNAL( focusChanged( int ) ), this, SLOT( focusChanged( int ) ) );
	QObject::connect( pathModelLayer, SIGNAL( selectedPointChanged( PathModel*, int ) ), this, SLOT( selectedPointChanged( PathModel*, int ) ) );	
}

TraectoryPlaner::~TraectoryPlaner()
{
	//writeSettings();	
}

void TraectoryPlaner::exportToGPX()
{
	QString PathToFile = FileDialog->getSaveFileName( 0, "Save", "", "*.gpx" ) + ".gpx";
	ui.editWidget->getPathModel( 1 )->exportGPX( PathToFile );
}

void TraectoryPlaner::importFromGPX()
{
	QString PathToFile = FileDialog->getOpenFileName( 0, "Open", "", "*.gpx" );
	ui.editWidget->getPathModel( 1 )->importGPX( PathToFile );
}

void TraectoryPlaner::clickedMapTypeButtons()
{
	if( sender()->objectName() == "mapButton" )
	{
		ui.satelliteButton->setChecked( false );
		ui.mapButton->setChecked( true );
		ui.mapControl->setMapAdapter( &openstreetMapAdapter );
	}
	else
	{
		ui.mapButton->setChecked( false );
		ui.satelliteButton->setChecked( true );
		ui.mapControl->setMapAdapter( &googleMapAdapter );
	}
}

void TraectoryPlaner::selectedPointChanged( PathModel* path, int index )
{
	if( path == ui.editWidget->getPathModel( 0 ) )
	{
		ui.tabWidget->setCurrentIndex( 0 );
		ui.tableBarrierPolygon->setFocus( index );
	}
	else if( path == ui.editWidget->getPathModel( 1 ) )
	{
		ui.tabWidget->setCurrentIndex( 1 );
		ui.tablePath->setFocus( index );
	}
}

void TraectoryPlaner::focusChanged( int index )
{
	if( sender() == ui.tableBarrierPolygon )
		pathModelLayer->selectPoint( ui.editWidget->getPathModel( 0 ), index );
	else
		pathModelLayer->selectPoint( ui.editWidget->getPathModel( 1 ), index );
}

void TraectoryPlaner::changedPathModel( PathModel* pathModel )
{
	pathModelLayer->setActivePathModel( pathModel );
}

void TraectoryPlaner::menuSettingsTriggered()
{
	writeSettings();
	SettingsWidget sw( this );
	sw.setModal( true );
	sw.show();
	sw.exec();

	barrierStyle.updateColors();
	trajectoryStyle.updateColors();
	readSettings();
}

void TraectoryPlaner::menuLoadTriggered()
{
	QByteArray input;
	int offset = 0, cout = 0;
	QString fileName = QFileDialog::getOpenFileName( 0, "Open path", "", "*.pm" );
	if( fileName.size() <= 0 )
		return;
	currentPMFile = fileName;
	QFile file( fileName );
	file.open( QIODevice::ReadOnly );
	input = file.readAll();
	while( input.size() > offset )
		offset += ui.editWidget->getPathModel( cout++ )->loadFromByteArray( input, offset );
}

void TraectoryPlaner::menuSaveTriggered()
{
	QByteArray output;
	if( currentPMFile.isEmpty() )
	{
		menuSaveAsTriggered();
		return;
	}
	uint cout = ui.editWidget->getPathModelCount();
	for( uint i = 0; i < cout; i++ )
		output.append( ui.editWidget->getPathModel( i )->saveToByteArray() );
	QFile file( currentPMFile );
	file.open( QIODevice::WriteOnly );
	file.write( output );
	file.close();
}

void TraectoryPlaner::menuSaveAsTriggered()
{
	QByteArray output;
	QString fileName = QFileDialog::getSaveFileName( 0, "Save path", "", "*.pm" );
	if( fileName.size() <= 0 )
		return;
	currentPMFile = fileName + ".pm";
	uint cout = ui.editWidget->getPathModelCount();
	for( uint i = 0; i < cout; i++ )
		output.append( ui.editWidget->getPathModel( i )->saveToByteArray() );
	QFile file( fileName + ".pm" );
	file.open( QIODevice::WriteOnly );
	file.write( output );
	file.close();
}

void TraectoryPlaner::closeEvent( QCloseEvent * CloseEvent )
{
	writeSettings();
	CloseEvent->accept();
}

MapAdapter* TraectoryPlaner::getMapAdapter()
{
	return ui.mapControl->getMapAdapter();
}

void TraectoryPlaner::writeSettings()
{
	QSettings settings( "settings.ini", QSettings::IniFormat );

	STATE state = ui.mapControl->getState( );
	settings.beginGroup( "Map" );
	settings.setValue( "zoom", state.zoom );
	settings.setValue( "position", state.pos );
	settings.setValue( "type", ( int )ui.mapButton->isChecked() );
	settings.endGroup( );
}

void TraectoryPlaner::readSettings()
{
	STATE state;
	QSettings settings( "settings.ini", QSettings::IniFormat );

	settings.beginGroup( "Map" );
	state.zoom = settings.value( "zoom" ).toInt( );
	state.pos = settings.value( "position" ).toPoint( );
	ui.mapControl->setMapAdapter( settings.value( "type" ).toInt() ? ( MapAdapter* )&openstreetMapAdapter : ( MapAdapter* )&googleMapAdapter );
	ui.mapButton->setChecked( settings.value( "type" ).toInt() == 1 );
	ui.satelliteButton->setChecked( settings.value( "type" ).toInt() == 0 );
	ui.mapControl->setState( state );
	settings.endGroup( );

	settings.beginGroup( "Cache" );
	imageManager.setCacheLimit( settings.value( "cacheLimit" ).toInt() );
	imageManager.setCacheDirPath( settings.value( "path" ).toString() );
	imageManager.setRAMLimit( settings.value( "ramLimit" ).toInt() );
	settings.endGroup( );
}

void TraectoryPlaner::resizeEvent( QResizeEvent* e )
{
	ui.mapTypeWidget->setGeometry( ui.mapControl->size().width() - 3 - ui.mapTypeWidget->size().width(), 3, ui.mapTypeWidget->size().width(), ui.mapTypeWidget->size().height() );
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

BarrierStyle::BarrierStyle()
{
	//updateColors();
}

void BarrierStyle::updateColors()
{
	QSettings s( "settings.ini", QSettings::IniFormat );
	s.beginGroup( "Colors" );
	
	pointColor = s.value( "BarrierPoint" ).value<QColor>();
	onHoverPointColor = s.value( "BarrierPointOnHover" ).value<QColor>();
	selectedPointColor = s.value( "BarrierSelectedPoint" ).value<QColor>( );

	lineColor = s.value( "BarrierLine" ).value<QColor>();
	onHoverLineColor = s.value( "BarrierLineOnHover" ).value<QColor>( );
	selectedLineColor = s.value( "BarrierSelectedLine" ).value<QColor>();

	s.endGroup();
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

TrajectoryStyle::TrajectoryStyle()
{
	//updateColors();
}

void TrajectoryStyle::updateColors()
{
	QSettings s( "settings.ini", QSettings::IniFormat );
	s.beginGroup( "Colors" );

	pointColor = s.value( "TraectoryPoint" ).value<QColor>();
	onHoverPointColor = s.value( "TraectoryPointOnHover" ).value<QColor>( );
	selectedPointColor = s.value( "TraectorySelectedPoint" ).value<QColor>( );

	lineColor = s.value( "TraectoryLine" ).value<QColor>();
	onHoverLineColor = s.value( "TraectoryLineOnHover" ).value<QColor>( );
	selectedLineColor = s.value( "TraectorySelectedLine" ).value<QColor>();

	s.endGroup();
}