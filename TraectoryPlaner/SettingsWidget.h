#pragma once

#include "std.h"
#include "ui_SettingsWidget.h"

class SettingsWidget : public QDialog
{
	Q_OBJECT

public:
	SettingsWidget( QWidget* parent = NULL );
	~SettingsWidget();

private slots:
	void tabChanged();
	void reset();
	void apply();
	
	void setCacheDirectory();

	void setBarrierLineColor();
	void setBarrierPointColor();
	void setBarrierOnHoverPointColor();
	void setBarrierSelectedLineColor();
	void setBarrierOnHoverLineColor();
	void setBarrierSelectedPointColor();

	void setTraectoryLineColor();
	void setTraectorySelectedLineColor();
	void setTraectoryPointColor();
	void setTraectoryOnHoverPointColor();
	void setTraectoryOnHoverLineColor();
	void setTraectorySelectedPointColor();

private:
	void readSettings();
	void writeSetting();

	void updateButtonColor( QPushButton*, QColor );
	void updateColor( QColor&, QColor& );

private:
	Ui::SettingsWidget ui;
	QDir* cacheDir;
	QString cacheDirPath;
	QPixmap* buttonColor;
	double width;
	long cacheLimit, ramLimit;


	QColor BarrierPointColor;
	QColor BarrierOnHoverPointColor;
	QColor BarrierSelectedPointColor;
	QColor BarrierLineColor;
	QColor BarrierOnHoverLineColor;
	QColor BarrierSelectedLineColor;
	
	QColor TraectoryPointColor;
	QColor TraectoryOnHoverPointColor;
	QColor TraectorySelectedPointColor;
	QColor TraectoryLineColor;
	QColor TraectoryOnHoverLineColor;
	QColor TraectorySelectedLineColor;
};