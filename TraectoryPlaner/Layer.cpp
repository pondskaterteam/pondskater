#include "Layer.h"

Layer::Layer()
{
	mapControl = NULL;
	pixmap = NULL;
}

Layer::~Layer()
{
}

void Layer::setMinSize( QSize size )
{
	if( mapControl )
		mapControl->setMinLayerSize( this, size );
}

void Layer::setLayerSize( QSize size )
{
	if( !pixmap )
		pixmap = new QPixmap( size );
	else if( pixmap->size() != size )
	{
		delete pixmap;
		pixmap = new QPixmap( size );
	}

	pixmap->fill( QColor( 0, 0, 0, 0 ) );
	if( mapControl->mapAdapter )
		draw();
}