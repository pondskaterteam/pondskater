#pragma once

#include "std.h"

class MouseEvent
{
public:
	MouseEvent( Qt::MouseButtons, QPoint );
	~MouseEvent(){}

	Qt::MouseButtons button();
	QPoint pos();

private:
	Qt::MouseButtons b;
	QPoint p;
};

class MouseDragEvent
{
public:
	MouseDragEvent( Qt::MouseButtons, QPoint last, QPoint current );
	~MouseDragEvent(){}

	Qt::MouseButtons button();
	QPoint last();
	QPoint current();
	QPoint shift();

private:
	Qt::MouseButtons b;
	QPoint l;
	QPoint c;
};