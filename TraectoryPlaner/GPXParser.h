#pragma once
#include "std.h"

class GPXParser : public QXmlDefaultHandler
{
public:
	GPXParser();
	bool startElement( const QString &namespaceURL, 
					   const QString &localName, 
					   const QString &qName, 
					   const QXmlAttributes &attributes);
	bool fatalError  ( const QXmlParseException &exeption);
	bool characters(const QString & strText);
	bool endElement(const QString&, const QString&, const QString& str);
	void copyCheckPoints( QVector <QPointF> &points);
private:
	QVector <QPointF> checkPoints;
};