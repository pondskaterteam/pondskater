#pragma once

#include "std.h"
#include "GPXParser.h"
#include "Area.h"

class PathModel : public QObject
{
	Q_OBJECT

public:
	PathModel();
	~PathModel();

	QVector< QPointF > getCheckPoints();
	void setCheckPoints( QVector< QPointF >& );

	void addCheckPoint( QPointF );
	void eraseCheckPoint( int index );
	void setCheckPoint( int index, QPointF );
	void insertCheckPoint( int index, QPointF );
	void changeIndex( int currentIndex, int newIndex );
	QPointF getCheckPoint( int index );
	void importGPX(QString pathToFile);
	void exportGPX(QString pathToFile);
	int getNumberCheckPoints();
	void pathClose( bool );
	bool isPathClosed();
	void clear();
	bool isCrossing();

	QByteArray saveToByteArray();
	int loadFromByteArray( QByteArray& byteArray, int offsetBytes = 0 ); // ���������� ���������� ��������� ����

private: signals:
	void changed();
	void changed( int index );
	void changedClosureProp();
	void inserted( int position, int count );
	void removed( int position, int count );
	void moved( int from, int to, int count );

private:
	QVector< QPointF > checkPoints;
	bool closed;
};
