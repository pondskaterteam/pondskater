#pragma once

#include "TableWidget.h"
#include "ui_CheckpointItemWidget.h"
#include "PathModel.h"

class CheckpointItemAllocator : public ItemWidgetAllocator
{
public:
	ItemWidget* allocate( QWidget* parent );
	void deallocate( ItemWidget* );
};

class CheckpointItemWidget : public ItemWidget
{
	Q_OBJECT

public:
	CheckpointItemWidget( QWidget* parent );

	void updateItem();

private:
	void selected( bool );
	void paintEvent( QPaintEvent* );

private slots:
	void moveButtonClicked();
	void saveButtonClicked();
	void deleteButtonClicked();

private:
	Ui::itemWidget ui;
	bool select;
};