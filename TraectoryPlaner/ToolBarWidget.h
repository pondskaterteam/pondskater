#pragma once

#include "std.h"

enum ToolBarEvent { ToolBarAddModeOn, ToolBarAddModeOff, ToolBarInsertCheckPoint, ToolBarDeleteCheckPoint };

class ToolBarWidget : public QWidget
{
	Q_OBJECT

public:
	ToolBarWidget( QWidget* parent = 0 );
	~ToolBarWidget();

	void offAddMode();

signals:
	void toolBarEvent( ToolBarEvent );

private slots:
	void clickedAddCheckPointButton();
	void clickedInsertCheckPointButton();
	void clickedDeleteCheckPointButton();

private:
};