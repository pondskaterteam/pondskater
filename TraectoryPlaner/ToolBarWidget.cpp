#include "ToolBarWidget.h"

ToolBarWidget::ToolBarWidget( QWidget* parent ) : QWidget( parent )
{
}

ToolBarWidget::~ToolBarWidget()
{
}

void ToolBarWidget::offAddMode()
{
	QPushButton* button = findChild< QPushButton* >( "addCheckPointButton" );
	if( button->isChecked() )
		button->click();
}

void ToolBarWidget::clickedAddCheckPointButton()
{
	QPushButton* button = ( QPushButton* )sender();
	if( button->isChecked() )
		toolBarEvent( ToolBarAddModeOn );
	else
		toolBarEvent( ToolBarAddModeOff );
}

void ToolBarWidget::clickedInsertCheckPointButton()
{
	toolBarEvent( ToolBarInsertCheckPoint );
}

void ToolBarWidget::clickedDeleteCheckPointButton()
{
	toolBarEvent( ToolBarDeleteCheckPoint );
}