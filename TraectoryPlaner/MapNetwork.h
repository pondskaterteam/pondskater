#pragma once

#include "std.h"
#include "MapAdapter.h"

class MapNetwork : public QObject
{
    Q_OBJECT

public:
    MapNetwork();
    ~MapNetwork(){}

    virtual void setMapAdapter( MapAdapter* ) = 0;
    virtual void dowloadTile( TILE ) = 0;

signals:
    void tileDownloaded( TILE, QPixmap* );

protected:
    MapAdapter* mapAdapter;
};

class MapNetworkSocket : public MapNetwork
{
	Q_OBJECT

public:
    MapNetworkSocket();
    ~MapNetworkSocket();

	void setMapAdapter( MapAdapter* );

	void dowloadTile( TILE );

private slots:
	void connected();
	void readyRead();
	void disconnected();
	void error( QAbstractSocket::SocketError );

private:
	QMap< QTcpSocket*, QPair< TILE, QByteArray* > > tileMap;
};

// ������ � ����� ����� �������� Qt
class MapNetworkManaged : public MapNetwork
{
    Q_OBJECT

public:
    MapNetworkManaged();
    ~MapNetworkManaged();

    void setMapAdapter( MapAdapter* );

    void dowloadTile( TILE );

private slots:
    void replyFinished(QNetworkReply*);

private:
    QNetworkAccessManager *manager;
	QMap< TILE, int > counterDownloads;
};
