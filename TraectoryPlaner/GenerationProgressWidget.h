#pragma once

#include "std.h"
#include "ui_GenerationProgressWidget.h"
#include "PathModel.h"

class GenerationProgressWidget : public QDialog
{
	Q_OBJECT

public:
	GenerationProgressWidget( QWidget* parent = NULL );
	~GenerationProgressWidget();

	bool buildIsCompleted();

private slots:
	void progress( int );

private:
	Ui::generationProgressDialog ui;
	bool buildCompleted;
};