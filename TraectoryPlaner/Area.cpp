
#include "Area.h"
#include <QList.h>
#include <qset.h>
#include <qpair.h>
#include <qline.h>
#include <qvector.h>
#include <qpoint.h>
Area::Area(QList<QPointF> points)
{
	this->points = points;
};
QList< QList<int> > Area::divByConvexAreaInt(){
	QList<ConvexArea> rez;

	QSet<QPair<int, int> > path;
	QList< QList<int> > convexs;
	QList<int> potential_unconvex;
	for (int i = 0; i<points.size(); i++)
		potential_unconvex.append(i);

	while (potential_unconvex.size() > 0){
		Q_ASSERT(potential_unconvex.size() >= 3);
		int currentPoint = 0;
		QList<int> convex;
		int startPoint = 0;
		while (path.contains(QPair<int, int>(potential_unconvex[startPoint], potential_unconvex[startPoint + 1]))){

			startPoint++;
			if (startPoint == potential_unconvex.size() - 1)
				return convexs;
		}
		convex.append(potential_unconvex[startPoint]);
		convex.append(potential_unconvex[startPoint + 1]);

		int k = potential_unconvex.size();
		bool last = true;
		for (int i = (startPoint + 2) % k; i != startPoint; i = (i + 1) % k)
		{

			//���� ������ ���� ��� ����������.
			if (path.contains(QPair<int, int>(convex[convex.size() - 1], potential_unconvex[i]))){
				continue;
			}
			last = false;
			convex.append(potential_unconvex[i]);


			if (!isConvex(convex) || !fullInArea(convex)){
				convex.pop_back();
				continue;
			}
			for (int i = 0; i < convexs.size(); i++){
				if (crossSubArea(convex, convexs[i])){
					convex.pop_back();
					break;
				}
			}

		}
		if (last)
			break;
		QList<QPointF> mypoints;
		int n = convex.size();
		int m = points.size();

		for (int j = 0; j < n; j++){

			mypoints.append(points[convex[j]]);
			path.insert(QPair<int, int>(convex[j], convex[(j + 1) % n]));


		}

		convexs.append(convex);
	
	}
	return convexs;

}
QList<ConvexArea> Area::divByConvexArea(){
	QList<ConvexArea> rez;
	QList< QList<int> > c = divByConvexAreaInt();
	for (int i = 0; i < c.size(); i++){
		QList<QPointF> d;
		for (int j= 0; j < c[i].size(); j++)
			d.append(points[c[i][j]]);

		rez.append(ConvexArea(d));
	}
	return rez;
}
//��������. �� ���������� �� ���� ����� ������ �����

bool Area::crossLine(int a, int b){
	int n = points.size();
	for (int i = 0; i < n; i++){
		QPointF pa = points[i];
		QPointF pb = points[(i + 1) % n];
		if (checkLine(pa, pb, points[a], points[b])){
			return true;
		}
	}
	return false;
}
QList<QPointF> Area::getPoints(){
	return points;
}
bool Area::checkLine(QPointF a, QPointF b, QPointF c,QPointF d){
	double A1 = b.y() - a.y();
	double B1 = a.x() - b.x();
	double C1 = -a.x()*A1 - a.y()*B1;

	double A2 = d.y() - c.y();
	double B2 = c.x() - d.x();
	double C2 = -c.x()*A2 - c.y()*B2;

	if ((A1*c.x() + B1*c.y() + C1)*(A1*d.x() + B1*d.y() + C1) >= 0)
		return false;
	if ((A2*a.x() + B2*a.y() + C2)*(A2*b.x() + B2*b.y() + C2) >= 0)
		return false;
	return true;
}

bool Area::checkLineAndBorder(QPointF a, QPointF b, QPointF c, QPointF d){
	double A1 = b.y() - a.y();
	double B1 = a.x() - b.x();
	double C1 = -a.x()*A1 - a.y()*B1;

	double A2 = d.y() - c.y();
	double B2 = c.x() - d.x();
	double C2 = -c.x()*A2 - c.y()*B2;

	if ((A1*c.x() + B1*c.y() + C1)*(A1*d.x() + B1*d.y() + C1) > 0)
		return false;
	if ((A2*a.x() + B2*a.y() + C2)*(A2*b.x() + B2*b.y() + C2) > 0)
		return false;
	return true;
}
bool Area::onLine(QPointF a, QPointF b, QPointF c){
	double A1 = b.y() - a.y();
	double B1 = a.x() - b.x();
	double C1 = -a.x()*A1 - a.y()*B1;
	return fabs(A1*c.x() + B1*c.y() + C1) < 1e-6;
}
bool Area::inArea(QPointF point,bool online){
	int n = points.size();
	int lastSign = 0;
	int num=0;
	for (int i = 0; i < n; i++){
		QPointF a = points[i];
		QPointF b = points[(i + 1) % n];
		//���� �� �����, �� �������, ��� �� ������
		if (online){
			if (onLine(a, b, point))
				return true;
			if (checkLineAndBorder(a, b, point, QPointF(point.x() + 123e3, 1e9)))
				num++;
		}
		else{
			if (onLine(a, b, point))
				return false;
			if (checkLineAndBorder(a, b, point, QPointF(point.x() + 123e3, 1e9)))
				num++;

		}
		

	}
	
	for (int i = 0; i < n; i++){
		if (onLine(point, QPointF(point.x() + 123e3, 1e9), points[i]))
			num++;
	}
	if (num % 2 == 0)
		return false;
	return true;
}
//��������, ����� �� ���� ��������� ������ ������
bool Area::inArea(int a, int b){
	if (crossLine(a, b) || !inArea((points[a] + points[b]) / 2)){
		return false;
	}
	return true;
}

Area::Area(){

}
//�������� ������������
bool Area::crossSubArea(QList<int> fig1, QList<int> fig2){
	int n1 = fig1.size();
	int n2 = fig2.size();
	Area a1(this, fig1);
	Area a2(this, fig2);
	for (int i = 0; i < n1; i++){
		for (int j = 0; j < n2; j++){
			int i1 = (i + 1) % n1;
			int j1 = (j + 1) % n2;
			QPointF m1 = (points[fig1[i]]+ points[fig1[i1]])/2;
			QPointF m2 = (points[fig2[j]] + points[fig2[j1]]) / 2;
			if (a1.inArea(m2, false))
				return true;
			if (a2.inArea(m1, false))
				return true;

			if (checkLine(points[fig1[i]], points[fig1[i1]], points[fig2[j]], points[fig2[j1]]))
				return true;
		}
	}
	
	
	for (int i = 0; i < n2; i++){
		if (a1.inArea(points[fig2[i]],false))
			return true;
	}
	for (int i = 0; i < n1; i++){
		if (a2.inArea(points[fig1[i]], false))
			return true;
	}

	return false;
}
//�������� ����� �� ��������� ������ ����� ������
bool Area::fullInArea(QList<int> testPoints){
	int n = testPoints.size();
	for (int i = 0; i < n; i++)
	if (!inArea(testPoints[i], testPoints[(i + 1) % n])){
		return false;
	}
	return true;
}
//�������� �� ����������
bool Area::isConvex(QList<int> testPoints){
	int n = testPoints.size();
	int zn = 0;
	for (int i = 0; i < n; i++){
		//������������ �������� �������� ������ ����� ���������� ����
		QPointF a = points[testPoints[(i + 1) % (n)]] - points[testPoints[i]];
		QPointF b = points[testPoints[(i + 2) % (n)]] - points[testPoints[(i + 1) % (n)]];
		double mul = a.x()*b.y() - b.x()*a.y();
		int currSign = (mul>0) ? 1 : ((mul<0) ?- 1:0);
		if (zn == 0)
			zn = currSign;
		else
		if (zn *currSign<0)
				return false;

	}
	return true;
}

Area::~Area()
{
}
Area::Area(Area* from, QList<int> v){
	for (int i = 0; i < v.size(); i++)
		points.append(from->points[v[i]]);
}
bool Area::isConvex(){
	QList<int> a;
	for (int i = 0; i < points.size(); i++){
		a.append(i);
	}
	return isConvex(a);
}
QList<int> Area::getPath(int i1, int i2){
	int n = points.size();
	double l1 = 0;
	QList<int> ans;
	ans.append(i1);
	for (int i = i1; i != i2; i=(i+1)%n){
		l1 += QLineF(points[i], points[(i - 1 + n) % n]).length();
	}
	double l2 = 0;
	for (int i = i2; i != i1; i = (i + 1) % n){
		l2 += QLineF(points[i], points[(i - 1 + n) % n]).length();
	}
	if (l1 < l2){
		for (int i = (i1 + 1) % n; i != i2; i = (i + 1) % n){
			ans.append(i);
		}
	}
	else{
		for (int i = (i1 + 1) % n; i != i2; i = (i - 1+n) % n){
			ans.append(i);
		}
	}
	ans.append(i2);
	return ans;
}
QList<QPointF> Area::getGals(double step){
	QList<QPointF> ans;

	QList<QList<int>> r = divByConvexAreaInt();
	QList<ConvexArea> r2 = divByConvexArea();

	for (int i = 0; i < r.length(); i++)
	{
		QList<QPointF> path=r2[i].getGals(step);
		for (int j = 0; j < path.length(); j++)
			ans.append(path[j]);
		if (i != r.length() - 1)
		{
			QList<int> path=getPath(r[i][0], r[i + 1][0]);
			for (int i = 0; i < path.size() - 1; i++)
			{
				ans.append(points[path[i]]);
			}
		}
		

	}
	return ans;
}
Area::Area(QVector<QPointF> v){
	QList<QPointF> t;
	for (int i = 0; i < v.size(); i++)
		t.append(v[i]);
	this->points = t;
}
bool Area::isCrossing(){
	
	int n = points.size();
	if (n < 3)
		return true;
	for (int i = 0; i <n ; i++){
		for (int j = i + 2; j < n; j++)
		if (i!=0||j!=n-1)
		{
			{
				QLineF l1(points[i], points[(i + 1) % n]);
				QLineF l2(points[j], points[(j + 1) % n]);
				QPointF p;
				QLineF::IntersectType d = l1.intersect(l2,&p);
				if (d == QLineF::IntersectType::BoundedIntersection)
					return true;

			}
		}
	}
	return false;
}