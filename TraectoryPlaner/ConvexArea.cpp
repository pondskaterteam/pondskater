
#include "ConvexArea.h"
#include "qline.h"
#define M_PI 3.142
ConvexArea::ConvexArea() :Area(){

}
ConvexArea::ConvexArea(QList<QPointF> v) :Area(v)
{
	if (!isConvex()){
		throw 1;
	}
}
double ConvexArea::square(){
	
	double s = 0;
	
	//���������� ���� ������� ������ ��� n-2 ������������� ������� ���������� ��� �������� �������������
	for (int i = 1; i < points.size() - 1; i++){
		QLineF a(points[0], points[i]);
		QLineF b(points[0], points[i+1]);
		QLineF c(points[i], points[i+1]);
		double p = (a.length() + b.length() + c.length()) / 2;
		s += sqrt(p*(p - a.length())*(p - b.length())*(p - c.length()));
	}

	return s;
}

ConvexArea::~ConvexArea()
{
}
QList<QPointF> ConvexArea::getGals(double step){
	ConvexArea ca = *this;
	QList<QPointF> ans = points;
	ans.append(points[0]);
	int p = 1;
	int n = points.length();
	while (ca.square()>step*step*6){
		QLineF c1(points[p], points[(p + 1) % points.length()]);
		QLineF c2(points[(p + 1) % points.length()], points[(p + 2) % points.length()]);


		ans.append(ca.movePoint(p, step ));
		
		
		p = (p + 1) % n;

	}
	return ans;
}
double ConvexArea::getLengthOfSide(int i){
	QLineF c(points[i], points[(i + 1) % points.length()]);
	return c.length();
}
QPointF ConvexArea::movePoint(int i, double step){
	QPointF v1 = points[(i+1) % points.length()] - points[i];
	v1 = v1 / (QLineF(points[i], points[(i + 1) % points.length()])).length();
	
	QPointF v2 = points[(i - 1 + points.size()) % points.length()] - points[(i) % points.length()];
	v2 = v2 / (QLineF(points[(i -1+points.size()) % points.length()], points[(i ) % points.length()])).length();
	QPointF v = (v1 + v2) / 2;
	v = v / QLineF(0, 0, v.x(), v.y()).length();
	double p1=v1.x()*v.x() + v1.y()*v.y();
	double p2 = v2.x()*v.x() + v2.y()*v.y();
	double p11 = sqrt(1 - p1*p1);
	double p22 = sqrt(1 - p2*p2);
	if (p22 > p11)
		p11 = p22;
	if (p11 < 0.2)
		p11 = 0.2;
	if (v.manhattanLength()>3)
		p11 = 1;
	points[i] += v*step/p11;
	return points[i];
}