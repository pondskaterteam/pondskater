#include "MapAdapter.h"

QString GoogleMapAdapter::GetName()
{
	return "Google";
}

QString GoogleMapAdapter::GetHost()
{
	return "mt1.google.com";
}

QString GoogleMapAdapter::GetURL(uint zoom, uint x, uint y)
{
    QString request = QString("http://%1/vt/lyrs=y&x=%2&y=%3&z=%4")
            .arg(GetHost()).arg(x).arg(y).arg(zoom);
    return request;
}

QString GoogleMapAdapter::GetRequestString( uint zoom, uint x, uint y )
{
    QString request = QString("GET /vt/lyrs=y&x=%1&y=%2&z=%3 HTTP/1.1\r\n")
            .arg(x).arg(y).arg(zoom);
    request.append("User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64)\r\n");
    request.append("Connection: close\r\n");
    request.append( QString("Host:%1\r\n").arg(GetHost()) );
    request.append("Accept: */*\r\n\r\n");

    return request;
}

QSize GoogleMapAdapter::GetTileSize()
{
	return QSize( 256, 256 );
}

QString GoogleMapAdapter::GetTileFormat()
{
	return "jpg";
}

uint GoogleMapAdapter::GetMaxZoom()
{
	return 20;
}

uint GoogleMapAdapter::GetMinZoom()
{
	return 0;
}

 QPoint GoogleMapAdapter::TransformLonLatToXY( QPointF coord, uint zoom )
{
	QPointF temp( ( double )( coord.x() + 180.0 ) / 360.0, ( 1.0 - log( tan( ( double )coord.y() * M_PI / 180.0 ) + ( double )1.0 / cos( ( double )coord.y() * M_PI / 180.0 ) ) / M_PI ) / 2.0 );
	return QPoint( temp.x() * ( 1 << zoom ) * 256, temp.y() * ( 1 << zoom ) * 256 );
}

QPointF GoogleMapAdapter::TransformXYToLonLat( QPoint coord, uint zoom )
{
	double d = 1.0 / ( double )( 1 << zoom ) / 256.0;
	double n = M_PI - 2.0 * M_PI * ( double )coord.y() * d;
	return QPointF( ( double )coord.x() * d * 360.0 - 180, 180.0 / M_PI * atan( 0.5 * ( exp( n ) - exp( -n ) ) ) );
}

QPointF GoogleMapAdapter::TransformLonLatToM(QPointF coord)
{	
	return QPointF(coord.x() *111321.38*cos(coord.y() / 180 * M_PI), coord.y() * 111321.38);
}

QPointF GoogleMapAdapter::TransformMToLonLat(QPointF coord)
{
	double y = (double)(coord.y()) / 111321.38;
	double m = 111321.38*cos(y / 180.0 * M_PI);

	double x = (double)(coord.x()) /m;
	
	return QPointF(x,y);

}

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------

QString OpenStreetMapAdapter::GetName()
{
	return "OpenStreetMap";
}

QString OpenStreetMapAdapter::GetHost()
{
	return "tile.openstreetmap.org";
}

QString OpenStreetMapAdapter::GetURL( uint zoom, uint x, uint y )
{
	QString request = QString( "http://%1/%2/%3/%4.png" )
		.arg( GetHost() ).arg( zoom ).arg( x ).arg( y );
	return request;
}

QString OpenStreetMapAdapter::GetRequestString( uint zoom, uint x, uint y )
{
	return "";
}

QSize OpenStreetMapAdapter::GetTileSize()
{
	return QSize( 256, 256 );
}

QString OpenStreetMapAdapter::GetTileFormat()
{
	return "png";
}

uint OpenStreetMapAdapter::GetMaxZoom()
{
	return 19;
}

uint OpenStreetMapAdapter::GetMinZoom()
{
	return 0;
}

QPoint OpenStreetMapAdapter::TransformLonLatToXY( QPointF coord, uint zoom )
{
	QPointF temp( ( double )( coord.x( ) + 180.0 ) / 360.0, ( 1.0 - log( tan( ( double )coord.y( ) * M_PI / 180.0 ) + ( double )1.0 / cos( ( double )coord.y( ) * M_PI / 180.0 ) ) / M_PI ) / 2.0 );
	return QPoint( temp.x( ) * ( 1 << zoom ) * 256, temp.y( ) * ( 1 << zoom ) * 256 );
}

QPointF OpenStreetMapAdapter::TransformXYToLonLat( QPoint coord, uint zoom )
{
	double d = 1.0 / ( double )( 1 << zoom ) / 256.0;
	double n = M_PI - 2.0 * M_PI * ( double )coord.y( ) * d;
	return QPointF( ( double )coord.x( ) * d * 360.0 - 180, 180.0 / M_PI * atan( 0.5 * ( exp( n ) - exp( -n ) ) ) );
}

QPointF OpenStreetMapAdapter::TransformLonLatToM( QPointF coord )
{
	return QPointF( coord.x( ) * 111321.38 * cos( coord.y( ) / 180 * M_PI ), coord.y( ) * 111321.38 );
}

QPointF OpenStreetMapAdapter::TransformMToLonLat( QPointF coord )
{
	double y = ( double )( coord.y() ) / 111321.38;
	double m = 111321.38 * cos( y / 180.0 * M_PI );

	double x = ( double )( coord.x() ) / m;

	return QPointF( x, y );
}