#include "MapStateLayer.h"

MapStateLayer::MapStateLayer()
{

}

MapStateLayer::~MapStateLayer()
{

}

void MapStateLayer::mapControlEvent()
{
	setMinSize( QSize( 500, 256 ) );
}

void MapStateLayer::draw()
{
	pixmap->fill( QColor( 0, 0, 0, 0 ) );
	STATE state = mapControl->getState();
	MapAdapter* mapAdapter = mapControl->getMapAdapter();

	QPainter painter;
	painter.begin( pixmap );
	painter.setPen( QPen( QColor( 0, 0, 0, 200 ) ) );
	painter.setBrush( QBrush( QColor( 0, 0, 0, 200 ) ) );
	QRect rect = QRect( 0, pixmap->size().height() - 30, pixmap->size().width(), 30 );
	painter.drawRect( rect );
	
	QPointF point = mapAdapter->TransformXYToLonLat( mousePos + state.pos, state.zoom );
	painter.setPen( QPen( QColor( 255, 255, 255, 255 ) ) );
	painter.setFont( QFont( "Calibri", 11 ) );
	rect.setWidth( pixmap->size().width() - 10 );
	painter.drawText( rect, QString( "%1Lon %2Lat" ).arg( point.x(), 0, 'g', 10 ).arg( point.y(), 0, 'g', 10 ), QTextOption( Qt::AlignRight | Qt::AlignVCenter ) );
	
	// ���������� ��������
	double a = 6378137, b = 6356752; // WGS84
	double lat = mapAdapter->TransformXYToLonLat( state.pos + QPoint( 10, pixmap->size().height() - 10 ), state.zoom ).y(); // ������� ����� ��� ���������� ������
	double Rlat = cos( lat * M_PI / 180 ) * sqrt( pow( a * cos( lat * M_PI / 180 ), 2 ) + pow( b* sin( lat * M_PI / 180 ), 2 ) );
	double scale = 2 * M_PI * Rlat / ( ( double )( 1 << state.zoom ) * mapAdapter->GetTileSize().width() );

	int m = scale * 200, n = 0;
	while( m / 10 )
	{
		m /= 10;
		n++;
	}
	while( m != 5 && m != 2 && m != 1 )
		m--;
	int length = ( double )m * pow( 10, n ) / scale;

	painter.setPen( QPen( QColor( 255, 255, 255, 220 ), 2 ) );
	painter.drawLine( QPoint( 10, rect.top() + 10 ), QPoint( 10, rect.top() + rect.height() - 10 ) );
	painter.drawLine( QPoint( 10, rect.top() + rect.height() - 10 ), QPoint( 10 + length, rect.top() + rect.height() - 10 ) );
	painter.drawLine( QPoint( 10 + length, rect.top() + 10 ), QPoint( 10 + length, rect.top() + rect.height() - 10 ) );
	rect = QRect( 10 + length + 5, pixmap->size().height() - 30, pixmap->size().width(), 30 );
	if( n >= 3 )
		painter.drawText( rect, QString( "%1 km" ).arg( m * pow( 10, n - 3 ), 0, 'g', 10 ), QTextOption( Qt::AlignLeft | Qt::AlignVCenter ) );
	else
		painter.drawText( rect, QString( "%1 m" ).arg( m * pow( 10, n ), 0, 'g', 10 ), QTextOption( Qt::AlignLeft | Qt::AlignVCenter ) );

	painter.end();
}

void MapStateLayer::mapAdapterEvent()
{

}

bool MapStateLayer::mouseMoveEvent( MouseEvent* e )
{
	mousePos = e->pos();
	draw();
	mapControl->update();
	return false;
}

bool MapStateLayer::mouseDragEvent( MouseDragEvent* e )
{
	mousePos = e->current();
	draw();
	mapControl->update();
	return false;
}