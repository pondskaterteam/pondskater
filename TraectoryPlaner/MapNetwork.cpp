#include "MapNetwork.h"

char* SearchStrForward( char* data, char* str );

char* SeekData( char* data );

MapNetwork::MapNetwork()
{
    mapAdapter = NULL;
}

MapNetworkSocket::MapNetworkSocket()
{
}

MapNetworkSocket::~MapNetworkSocket()
{
	for( QMap< QTcpSocket*, QPair< TILE, QByteArray* > >::iterator i = tileMap.begin(); i != tileMap.end(); ++i )
	{
		i.key()->close();
		delete i.key();
		delete i.value().second;
	}
}

void MapNetworkSocket::setMapAdapter( MapAdapter* mapAdapter )
{
	for( QMap< QTcpSocket*, QPair< TILE, QByteArray* > >::iterator i = tileMap.begin(); i != tileMap.end(); ++i )
	{
		i.key()->close();
		delete i.key();
		delete i.value().second;
	}
	this->mapAdapter = mapAdapter;
}

void MapNetworkSocket::dowloadTile( TILE tile )
{
	if( !mapAdapter )
		return;

	QTcpSocket* socket = new QTcpSocket;
	tileMap[socket] = QPair< TILE, QByteArray* >( tile, new QByteArray );

	QObject::connect( socket, SIGNAL( connected() ), this, SLOT( connected() ) );
	QObject::connect( socket, SIGNAL( readyRead() ), this, SLOT( readyRead() ) );
	QObject::connect( socket, SIGNAL( disconnected() ), this, SLOT( disconnected() ) );
	QObject::connect( socket, SIGNAL( error( QAbstractSocket::SocketError ) ), this, SLOT( error( QAbstractSocket::SocketError ) ) );
	
	socket->connectToHost( mapAdapter->GetHost(), 80 );
}

void MapNetworkSocket::connected()
{
	QTcpSocket* socket = ( QTcpSocket* )sender();
	TILE tile = tileMap[socket].first;
	QString request = mapAdapter->GetRequestString( tile.zoom, tile.x, tile.y );
	socket->write( request.toUtf8() );
}

void MapNetworkSocket::readyRead()
{
	QTcpSocket* socket = ( QTcpSocket* )sender();
	QByteArray* data = tileMap[socket].second;
	QByteArray newData = socket->read( 100 * 1024 );
	data->append( newData );
}

void MapNetworkSocket::disconnected()
{
	QTcpSocket* socket = ( QTcpSocket* )sender();
	TILE tile = tileMap[socket].first;
	QByteArray* byteArray = tileMap[socket].second;
	tileMap.erase( tileMap.find( socket ) );

	//�������� �������� �� ������� �������� � ������!
	char* data = byteArray->data();
	char* dataImage = SeekData( data );
	QPixmap* pixmap = new QPixmap;
	pixmap->loadFromData( ( uchar* )dataImage, byteArray->size() - ( dataImage - data ), mapAdapter->GetTileFormat().toUtf8().data() );

	delete byteArray;
	tileDownloaded( tile, pixmap );
}

void MapNetworkSocket::error( QAbstractSocket::SocketError error )
{
	if( error == QAbstractSocket::ConnectionRefusedError )
	{
		QTcpSocket* socket = ( QTcpSocket* )sender();
		delete tileMap[socket].second;
		delete socket;
	}
}

char* SearchStrForward( char* data, char* str )
{
	for( int i = 0, i2 = 0, reg = 0; data[i] != 0; i++, i2 = 0, reg = i )
	{
		while( data[i] == str[i2] )
		{
			i++;
			i2++;
			if( str[i2] == 0 )
				return data + reg;
		}
	}
	return 0;
}

char* SeekData( char* data )
{
	return SearchStrForward( data, "\r\n\r\n" ) + sizeof( "\r\n\r\n" ) - 1;
}

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------

extern bool operator < ( TILE a, TILE b );

MapNetworkManaged::MapNetworkManaged()
{
    manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(replyFinished(QNetworkReply*)));
}

MapNetworkManaged::~MapNetworkManaged()
{
}

void MapNetworkManaged::setMapAdapter(MapAdapter *mapAdapter)
{
    this->mapAdapter = mapAdapter;
	manager->clearAccessCache();
}

void MapNetworkManaged::dowloadTile(TILE tile)
{
    if( !mapAdapter )
        return;

	QNetworkRequest request( QUrl( mapAdapter->GetURL( tile.zoom, tile.x, tile.y ) ) );
	request.setHeader( QNetworkRequest::KnownHeaders::UserAgentHeader, "Mozilla/5.0 (Windows NT 6.1; WOW64" );

	// �������, ����� ���������
	request.setAttribute( QNetworkRequest::User, tile.zoom );
	request.setAttribute( ( QNetworkRequest::Attribute ) ( QNetworkRequest::User + 1 ), tile.x );
	request.setAttribute( ( QNetworkRequest::Attribute ) ( QNetworkRequest::User + 2 ), tile.y );

	manager->get( request );
}


void MapNetworkManaged::replyFinished( QNetworkReply *reply )
{
	TILE tile;

	// �������, ����� ���������
	tile.zoom = reply->request().attribute( QNetworkRequest::User ).toInt();
	tile.x = reply->request().attribute( ( QNetworkRequest::Attribute ) ( QNetworkRequest::User + 1 ) ).toInt();
	tile.y = reply->request().attribute( ( QNetworkRequest::Attribute ) ( QNetworkRequest::User + 2 ) ).toInt();

	QString req = mapAdapter->GetURL( tile.zoom, tile.x, tile.y );
	//int err;
	//qDebug() << "Error:" << ( err = reply->error() );

	if( reply->error() )
	{
		if( counterDownloads[tile] < 10 )
		{
			counterDownloads[tile]++;
			dowloadTile( tile );
		}
		else
		{
			counterDownloads.erase( counterDownloads.find( tile ) );
			return;
		}
	}

	QPixmap* pixmap = new QPixmap;
	pixmap->loadFromData( reply->readAll() );
	reply->deleteLater();

	if( pixmap->size().width() != 0 && pixmap->size().height() != 0 )
		tileDownloaded( tile, pixmap );
	else
		delete pixmap;
}


