#include "EditWidget.h"
#include "TraectoryPlaner.h"


EditWidget::EditWidget( QWidget* parent ) : QWidget( parent )
{
	roolUp = false;
	disableState[0] = disableState[1] = false;

	currentTab = -1;

	barrierPolygon.pathClose( true );
	path.pathClose( false );

	QObject::connect( &path, SIGNAL( changedClosureProp() ), this, SLOT( changedPathClosureProp() ) ); // ����������� ����� .pathClose()

	toolBarWidget = parent->findChild< ToolBarWidget* >( "toolBarWidget" );
}

EditWidget::~EditWidget()
{
}

uint EditWidget::getPathModelCount()
{
	return 2;
}

PathModel* EditWidget::getPathModel( uint index )
{
	if( index == 0 )
		return &barrierPolygon;
	else if( index == 1 )
		return &path;

	return NULL;
}

void EditWidget::blockingToolBar()
{
	if( currentTab == 0 )
		toolBarWidget->setEnabled( !disableState[0] );
	if( currentTab == 1 )
		toolBarWidget->setEnabled( !disableState[1] );
}

void EditWidget::clickedRollUpButton()
{
	QTabWidget* tabWidget = parent()->findChild< QTabWidget* >( "tabWidget" );
	if( roolUp )
	{
		tabWidget->show();
		this->setMinimumHeight( 220 );
		this->setMaximumHeight( 16777215 );
		roolUp = false;
	}
	else
	{
		tabWidget->hide();
		this->setFixedHeight( 45 );
		roolUp = true;
	}
}

void EditWidget::changedTab( int current )
{
	toolBarWidget->offAddMode();
	currentTab = current;
	blockingToolBar();

	if( !disableState[current] )
	{
		switch ( current )
		{
		case 0:
			changedPathModel( &barrierPolygon );
			break;
		case 1:
			changedPathModel( &path );
			break;
		default:
			changedPathModel( 0 );
			break;
		}
	}
	else
		changedPathModel( 0 );
}

void EditWidget::stateCheckBoxChanged( int state )
{
	QString name = sender()->objectName();
	if( name == "enabledBPCheckBox" )
		disableState[0] = ( bool )state;
	else if( name == "enabledPCheckBox" )
	{
		disableState[1] = ( bool )state;
		findChild< QCheckBox* >( "closedPathCheckBox" )->setEnabled( !state );
	}
	else if( name == "closedPathCheckBox" )
	{
		path.pathClose( state );
		return;
	}

	if( disableState[0] || disableState[1] )
		findChild< QPushButton* >( "generatePathButton" )->setEnabled( false );
	else
		findChild< QPushButton* >( "generatePathButton" )->setEnabled( true );

	if( disableState[currentTab] )
	{
		toolBarWidget->offAddMode();
		switch( currentTab )
		{
		case 0:
			barrierPolygon.clear();
			break;
		case 1:
			path.clear();
			break;
		}
	}

	blockingToolBar();
}

void EditWidget::changedPathClosureProp()
{
	findChild< QCheckBox* >( "closedPathCheckBox" )->setChecked( ( static_cast< PathModel* >( sender() ) )->isPathClosed() );
}

void EditWidget::clickedGenerateTraectory()
{
	if( getPathModel( 0 )->isCrossing( ) )
	{
		QMessageBox msgBox;
		msgBox.setText( QString::fromLocal8Bit( "������ ���������� ����. ��������� ����������" ) );
		msgBox.exec( );
		return;
	}

	GenerationProgressWidget wp( this );
	wp.setModal( true );
	wp.show();	

	GenerationThread generationThread( getPathModel( 0 )->getCheckPoints(), ( static_cast< TraectoryPlaner* >( parent()->parent() ) )->getMapAdapter() );
	QObject::connect( &generationThread, SIGNAL( progress( int ) ), &wp, SLOT( progress( int ) ) );

	generationThread.start();
	wp.exec();	

	if( !wp.buildIsCompleted() )
		generationThread.terminate(); // ��� ���� ��������
	else
		getPathModel( 1 )->setCheckPoints( generationThread.getResult() );
}

void EditWidget::clickedClearButton()
{
	if( sender()->objectName() == "clearBarrierButton" )
		barrierPolygon.clear();
	else
		path.clear();
}

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------

GenerationThread::GenerationThread( QVector< QPointF >& barrierPolygon, MapAdapter* mapAdapter )
{
	this->barrierPolygon = barrierPolygon;
	this->mapAdapter = mapAdapter;
}

void GenerationThread::run()
{
	buildPath( barrierPolygon, mapAdapter );
}

void GenerationThread::buildPath( QVector< QPointF >& checkPoints, MapAdapter* mapAdapter )
{
	progress( 50 );

	QSettings s( "settings.ini", QSettings::IniFormat );
	s.beginGroup( "Traectory" );

	QList<QPointF> rez;//��������� � �����
	for( int i = 0; i < checkPoints.size(); i++ )
		rez.append( mapAdapter->TransformLonLatToM( checkPoints[i] ) );

	QPointF ee = mapAdapter->TransformMToLonLat( mapAdapter->TransformLonLatToM( QPointF( 80, 80 ) ) );
	Area area( rez );
	double width = s.value( "width" ).toDouble();

	QList<QPointF> d = area.getGals( width / 10 );

	result.clear();//���������� �������
	for( int i = 0; i < d.size(); i++ ){
		result.append( mapAdapter->TransformMToLonLat( d[i] ) );
	}

	progress( 100 );
}

QVector< QPointF > GenerationThread::getResult( )
{
	return result;
}