#include "MapLayer.h"

bool operator < ( QPoint a, QPoint b )
{
	if( a.y() == b.y() )
		return a.x() < b.x();
	else
		return a.y() < b.y();
}

bool operator < ( QSize a, QSize b )
{
	if( a.height() == b.height() )
		return a.width() < b.width();
	else
		return a.height() < b.height();
}

class DefaultPixmap
{
public:
	QPixmap* get( QSize size )
	{
		if( pixmaps.contains( size ) )
			return pixmaps[size];

		QPixmap* pixmap = new QPixmap( size );
		pixmap->fill( QColor( 29, 34, 44, 255 ) );

		QPainter painter;
		painter.begin( pixmap );

		// ������
		painter.setPen( QColor( 22, 26, 33 ) );
		painter.drawLine( QPoint( 0, 0 ), QPoint( size.width() - 1, 0 ) );
		painter.drawLine( QPoint( size.width() - 1, 0 ), QPoint( size.width() - 1, size.height() - 1 ) );
		painter.drawLine( QPoint( size.width() - 1, size.height() - 1 ), QPoint( 0, size.height() - 1 ) );
		painter.drawLine( QPoint( 0, size.height() - 1 ), QPoint( 0, 0 ) );

		// ����������� �����
		painter.drawLine( QPoint( size.width() / 2 - 1, 0 ), QPoint( size.width() / 2 - 1, size.height() - 1 ) );
		painter.drawLine( QPoint( 0, size.height() / 2 - 1 ), QPoint( size.width() - 1, size.height() / 2 - 1 ) );

		// ��������� �����
		painter.setPen( QPen( QColor( 22, 26, 33 ), 1, Qt::DashLine ) );
		painter.drawLine( QPoint( size.width() / 2 - 1 - size.width() / 4, 0 ), QPoint( size.width() / 2 - 1 - size.width() / 4, size.height() - 1 ) );
		painter.drawLine( QPoint( size.width() / 2 - 1 + size.width() / 4, 0 ), QPoint( size.width() / 2 - 1 + size.width() / 4, size.height() - 1 ) );
		painter.drawLine( QPoint( 0, size.height() / 2 - 1 - size.height() / 4 ), QPoint( size.width() - 1, size.height() / 2 - 1 - size.height() / 4 ) );
		painter.drawLine( QPoint( 0, size.height() / 2 - 1 + size.height() / 4 ), QPoint( size.width() - 1, size.height() / 2 - 1 + size.height() / 4 ) );

		painter.end();

		pixmaps[size] = pixmap;
		return pixmap;
	}

private:
	QMap< QSize, QPixmap* > pixmaps;
}defaultPixmap;

MapLayer::MapLayer() : Layer()
{
	imageManager = NULL;
    needRepaint = false;
}

MapLayer::~MapLayer()
{
	if( imageManager )
		imageManager->mapLayer = NULL;
}

void MapLayer::setImageManager( ImageManager* imageManager )
{
	this->imageManager = imageManager;
	imageManager->mapLayer = this;
}

void MapLayer::tileDownloaded( TILE tile, QPixmap* pixmap )
{
	/*STATE state = mapControl->getState();
	QPoint begin( state.pos.x() / mapControl->getMapAdapter()->GetTileSize().width(), state.pos.y() / mapControl->getMapAdapter()->GetTileSize().height() );
	QPoint end( ( state.pos.x() + pixmap->size().width() ) / mapControl->getMapAdapter()->GetTileSize().width(), ( state.pos.y() + pixmap->size().height() ) / mapControl->getMapAdapter()->GetTileSize().height() );
	if( tile.zoom == state.zoom && begin.x() <= tile.x &&  tile.x <= end.x() && begin.y() <= tile.y &&  tile.y <= end.y() )		
	{
		QPainter painter;
		painter.begin( pixmap );
		QPoint pos( tile.x * mapControl->getMapAdapter()->GetTileSize().width() - state.pos.x(), tile.y * mapControl->getMapAdapter()->GetTileSize().height() - state.pos.y() );
		painter.drawPixmap( pos, *pixmap );
		painter.end();
	}*/
	
	//��������
	draw();
    mapControl->update();
}

void MapLayer::draw()
{
	if( imageManager == NULL )
		return;

    // ������������� ��������� ��������� � ��������� �� ��� �� ����� ��������
    if (pixmap->paintingActive())
    {
        needRepaint = true;
        return;
    }

	STATE state = mapControl->getState();
	MapAdapter* mapAdapter = mapControl->getMapAdapter();

	QPoint begin( state.pos.x() / mapAdapter->GetTileSize().width(),
		state.pos.y() / mapAdapter->GetTileSize().height() );

	QPoint end( ( state.pos.x() + pixmap->size().width() - 1 ) / mapAdapter->GetTileSize().width(),
		( state.pos.y() + pixmap->size().height() - 1 ) / mapAdapter->GetTileSize().height() );


	QPainter painter;
	painter.begin( pixmap );
	for( int x = begin.x(); x <= end.x(); x++ )
	{
		for( int y = begin.y(); y <= end.y(); y++ )
		{
			QPixmap* pixmap = imageManager->getPixmapTile( TILE( state.zoom, x, y ) );
			QPoint pos( x * mapAdapter->GetTileSize().width() - state.pos.x(), y * mapAdapter->GetTileSize().height() - state.pos.y() );
			if( pixmap )
				painter.drawPixmap( pos, *pixmap );
			else
				painter.drawPixmap( pos, *defaultPixmap.get( mapAdapter->GetTileSize() ) );
		}
	}
	

	painter.end();

    if(needRepaint)
    {
        needRepaint = false;
        draw();
    }
}

void MapLayer::mapAdapterEvent()
{
	if( imageManager )
		imageManager->setMapAdapter( mapControl->getMapAdapter() );
	if( mapControl->getMapAdapter() )
	{
		draw();
		mapControl->update();
	}
}

bool MapLayer::mouseDragEvent( MouseDragEvent* e )
{
	//��������
	STATE state = mapControl->getState();
	state.pos -= e->shift();
	mapControl->setState( state );

	return true;
}

bool MapLayer::mouseMoveEvent( MouseEvent* e )
{
	mousePos = e->pos();

	return true;
}

bool MapLayer::wheelEvent( QWheelEvent* e )
{
	STATE state = mapControl->getState();
	if( e->angleDelta().y() > 0 )
		mapControl->setZoom( state.zoom + 1, mousePos );
	else
		mapControl->setZoom( state.zoom - 1, mousePos );

	return true;
}