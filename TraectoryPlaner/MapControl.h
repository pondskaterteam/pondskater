#pragma once

#include "std.h"
#include "Layer.h"
#include "MapAdapter.h"
#include "MouseEvents.h"

class Layer;

struct STATE
{
	STATE(){ zoom = 0; pos = QPoint( 0, 0 ); }	 

	uint zoom;
	QPoint pos;
};

class MapControl : public QWidget
{
	friend class Layer;

public:
	MapControl( QWidget* parent = NULL );
	~MapControl();

	void setMapAdapter( MapAdapter* );
	MapAdapter* getMapAdapter();
	void addLayer( Layer* );

	void setState( STATE );
	void setZoom( uint, QPoint = QPoint( 0, 0 ) );
	void setPos( QPoint );
	STATE getState();

private:
	void enterEvent( QEvent* );
	void leaveEvent( QEvent* );
	void mousePressEvent( QMouseEvent* );
	void mouseReleaseEvent( QMouseEvent* );
	void mouseMoveEvent( QMouseEvent* );
	void wheelEvent( QWheelEvent* );

	void paintEvent( QPaintEvent* );
	void resizeEvent( QResizeEvent* );

	void setMinLayerSize( Layer*, QSize );

private:
	STATE state;
	MapAdapter* mapAdapter;
	QVector< Layer* > vLayer;
	QMap< Layer*, QSize > layerSizeMap;
	QSize maxMinLayerSize;
	QSize layerSize;

	QPoint posMouse;
};