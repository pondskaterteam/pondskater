#include "ImageManager.h"

bool operator < ( TILE a, TILE b )
{
	if( a.zoom == b.zoom )
	{
		if( a.y == b.y )
			return a.x < b.x;
		else
			return a.y < b.y;
	}
	else
		return a.zoom < b.zoom;
}

void ImageManager::tileDownloaded( TILE tile, QPixmap* pixmap )
{
	if( mapLayer )
		mapLayer->tileDownloaded( tile, pixmap );
}

ImageManagerCaching::ImageManagerCaching( ) : ImageManager()
{
	mapAdapter = 0;
	QObject::connect( &mapNetwork, SIGNAL( tileDownloaded( TILE, QPixmap* ) ), this, SLOT( downloaded( TILE, QPixmap* ) ) );
}

ImageManagerCaching::~ImageManagerCaching()
{
}

void ImageManagerCaching::setMapAdapter( MapAdapter* mapAdapter )
{
	this->mapAdapter = mapAdapter;
	mapNetwork.setMapAdapter( mapAdapter );
	tilesInProcess.clear();
}

QPixmap* ImageManagerCaching::getPixmapTile( TILE tile )
{
	if( !mapAdapter )
		return NULL;

	QPixmap* pixmap = fileCache.load( keyValue( tile ) );
	if( !pixmap && !tilesInProcess.contains( tile ) )
	{
		tilesInProcess[tile];
		mapNetwork.dowloadTile( tile );
	}

	return pixmap;
}

void ImageManagerCaching::setCacheDirPath( QString dirPath )
{
	fileCache.setCacheDirPath( dirPath );
}

void ImageManagerCaching::setCacheLimit( int size )
{
	fileCache.setCacheLimit( size );
}

void ImageManagerCaching::setRAMLimit( int size )
{
	fileCache.setRamLimit( size );
}

void ImageManagerCaching::downloaded( TILE tile, QPixmap* pixmap )
{
	tilesInProcess.erase( tilesInProcess.find( tile ) );
	QString key = keyValue( tile );
	fileCache.insert( key, pixmap );
	tileDownloaded( tile, pixmap );
}

QString ImageManagerCaching::keyValue(TILE tile)
{
    return QString ("%1_%2_%3_%4.%5").arg( mapAdapter->GetName() ).arg(tile.zoom).arg(tile.x).arg(tile.y).arg(mapAdapter->GetTileFormat());
}