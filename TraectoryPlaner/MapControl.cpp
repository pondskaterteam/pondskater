#include "MapControl.h"

bool operator == ( STATE a, STATE b )
{
	if( a.pos == b.pos && a.zoom == b.zoom )
		return true;
	else
		return false;
}

QSize CalcLayerSize( QSize wndSize, uint zoom, MapAdapter* mapAdapter )
{
	QSize res = mapAdapter->GetTileSize() * ( 1 << zoom );
	if( wndSize.width() < res.width() )
		res.setWidth( wndSize.width() );
	if( wndSize.height() < res.height() )
		res.setHeight( wndSize.height() );

	return res;
}

void SetCorrectPos( STATE& state, QSize& layerSize, MapAdapter* mapAdapter )
{
	if( state.pos.x() < 0 )
		state.pos.setX( 0 );
	if( state.pos.y() < 0 )
		state.pos.setY( 0 );
	QSize maxSize = mapAdapter->GetTileSize() * ( 1 << state.zoom );
	if( state.pos.x() + layerSize.width() > maxSize.width() )
		 state.pos.setX( maxSize.width() - layerSize.width() );
	if( state.pos.y() + layerSize.height() > maxSize.height() )
		 state.pos.setY( maxSize.height() - layerSize.height() );
}
// ���������� ���������� ��� � ����������� �� ������������ ������� ����
uint CalcZoom( QSize minLayerSize, uint zoom, MapAdapter* mapAdapter ) 
{
	uint max = MAX( minLayerSize.width(), minLayerSize.height() );
	uint min = MIN( mapAdapter->GetTileSize().width(), mapAdapter->GetTileSize().height() );
	float minZoom = log( ( float )max / min ) / log( 2.0f );
	if( minZoom > ( int )minZoom )
		minZoom++;
	if( minZoom > zoom && mapAdapter->GetMinZoom() <= minZoom && minZoom <= mapAdapter->GetMaxZoom() )
		return ( uint )minZoom;
	else
		return zoom;
}

MapControl::MapControl( QWidget* parent ) : QWidget( parent )
{
	mapAdapter = NULL;
	maxMinLayerSize = QSize( 0, 0 );
	layerSize = QSize( 0, 0 );
	this->setMouseTracking( true );	
}

MapControl::~MapControl()
{	
}

void MapControl::setMapAdapter( MapAdapter* mapAdapter )
{
	this->mapAdapter = mapAdapter;
	resizeEvent( 0 );
	for( int i = 0; i < vLayer.size(); i++ )
		vLayer[i]->mapAdapterEvent();
}

MapAdapter* MapControl::getMapAdapter()
{
	return mapAdapter;
}

void MapControl::addLayer( Layer* layer )
{
	vLayer.push_back( layer );
	layer->mapControl = this;
	layer->setLayerSize( layerSize );
	layerSizeMap[layer] = QSize( 0, 0 );
	layer->mapControlEvent();
	layer->mapAdapterEvent();
}

void MapControl::setState( STATE state )
{
	if( this->state == state )
		return;

	state.zoom = CalcZoom( maxMinLayerSize, state.zoom, mapAdapter );	
	SetCorrectPos( state, layerSize, mapAdapter );

	if( this->state == state )
		return;

	this->state = state;

	QSize layerNewSize = CalcLayerSize( size(), state.zoom, mapAdapter);
	if( layerNewSize != layerSize )
	{
		layerSize = layerNewSize;
		for( int i = 0; i < vLayer.size(); i++ )
			vLayer[i]->setLayerSize( layerSize );
	}
	else
	{
		for( int i = 0; i < vLayer.size(); i++ )
			vLayer[i]->draw();
	}		

	//��������, � �������� � ���
    update();
}

void MapControl::setZoom( uint zoom, QPoint posZoom )
{
	if( zoom > mapAdapter->GetMaxZoom() || zoom < mapAdapter->GetMinZoom() )
		return;
		
	zoom = CalcZoom( maxMinLayerSize, zoom, mapAdapter );

	QPoint pos = posZoom + state.pos;
	state.pos.setX( ( float )pos.x() * ( 1 << zoom ) / ( 1 << state.zoom ) - posZoom.x() );
	state.pos.setY( ( float )pos.y() * ( 1 << zoom ) / ( 1 << state.zoom ) - posZoom.y() );
	state.zoom = zoom;

	QSize layerNewSize = CalcLayerSize( size(), zoom, mapAdapter);
	SetCorrectPos( state, layerNewSize, mapAdapter );
	if( layerNewSize != layerSize )		
	{
		layerSize = layerNewSize;
		for( int i = 0; i < vLayer.size(); i++ )
			vLayer[i]->setLayerSize( layerSize );
	}
	else
	{
		for( int i = 0; i < vLayer.size(); i++ )
			vLayer[i]->draw();
	}

	//��������, � �������� � ���
    update();
}

void MapControl::setPos( QPoint pos )
{
	if( state.pos == pos )
		return;

	state.pos = pos;
	SetCorrectPos( state, layerSize, mapAdapter );
	for( int i = 0; i < vLayer.size(); i++ )
			vLayer[i]->draw();

	//��������, � �������� � ���
    update();
}

STATE MapControl::getState()
{
	return state;
}

void MapControl::enterEvent( QEvent* )
{
	if( mapAdapter == 0 )
		return;

	bool res = false;
	for( int i = vLayer.size() - 1; !res && i >= 0; i-- )
		res = vLayer[i]->enterEvent();
}

void MapControl::leaveEvent( QEvent* )
{
	if( mapAdapter == 0 )
		return;

	bool res = false;
	for( int i = vLayer.size() - 1; !res && i >= 0; i-- )
		res = vLayer[i]->leaveEvent();
}

void MapControl::mousePressEvent( QMouseEvent* e )
{
	if( mapAdapter == 0 )
		return;

	QSize pos =  ( size() - layerSize ) / 2;
	QRect rect( pos.width(), pos.height(), layerSize.width(), layerSize.height() ); 
	if( !rect.contains( e->pos() ) )
		return;
	posMouse = e->pos() - QPoint( pos.width(), pos.height() );
	MouseEvent mouseEvent( e->buttons(), posMouse );
	bool res = false;
	for( int i = vLayer.size() - 1; !res && i >= 0; i-- )
		res = vLayer[i]->mousePressEvent( &mouseEvent );
}

void MapControl::mouseReleaseEvent( QMouseEvent* e )
{
	if( mapAdapter == 0 )
		return;

	QSize pos =  ( size() - layerSize ) / 2;
	QRect rect( pos.width(), pos.height(), layerSize.width(), layerSize.height() ); 
	if( !rect.contains( e->pos() ) )
		return;
	MouseEvent mouseEvent( e->buttons(), e->pos() - QPoint( pos.width(), pos.height() ) );
	bool res = false;
	for( int i = vLayer.size() - 1; !res && i >= 0; i-- )
		res = vLayer[i]->mouseReleaseEvent( &mouseEvent );
}

void MapControl::mouseMoveEvent( QMouseEvent* e )
{
	if( mapAdapter == 0 )
		return;

	QSize pos =  ( size() - layerSize ) / 2;
	QRect rect( pos.width(), pos.height(), layerSize.width(), layerSize.height() ); 
	if( !rect.contains( e->pos() ) )
		return;
	MouseEvent mouseEvent( e->buttons(), e->pos() - QPoint( pos.width(), pos.height() ) );
	bool res = false;
    if( e->buttons() == Qt::NoButton )
    {
		for( int i = vLayer.size() - 1; !res && i >= 0; i-- )
			res = vLayer[i]->mouseMoveEvent( &mouseEvent );
	}
	else
	{
		MouseDragEvent dragEvent( e->buttons(), posMouse, mouseEvent.pos() );
		posMouse = mouseEvent.pos();
		for( int i = vLayer.size() - 1; !res && i >= 0; i-- )
			res = vLayer[i]->mouseDragEvent( &dragEvent );
	}	
}

void MapControl::wheelEvent( QWheelEvent* e )
{
	if( mapAdapter == 0 )
		return;

	bool res = false;
	for( int i = vLayer.size() - 1; !res && i >= 0; i-- )
		res = vLayer[i]->wheelEvent( e );
}

void MapControl::paintEvent( QPaintEvent* e )
{
	if( mapAdapter == 0 )
		return;

	QPainter painter;
	painter.begin( this );
	QSize pos =  size() / 2 - layerSize / 2;
	for( int i = 0; i < vLayer.size(); i++ )
		painter.drawPixmap( pos.width(), pos.height(), *vLayer[i]->pixmap );
	painter.end();
}

void MapControl::resizeEvent( QResizeEvent* )
{
	if( mapAdapter == 0 )
		return;

	QSize layerNewSize = CalcLayerSize( size( ), state.zoom, mapAdapter );
	SetCorrectPos( state, layerNewSize, mapAdapter );
	if( layerNewSize != layerSize )
	{
		layerSize = layerNewSize;
		for( int i = 0; i < vLayer.size(); i++ )
			vLayer[i]->setLayerSize( layerSize );
	}
	else
        update();
}

void MapControl::setMinLayerSize( Layer* layer, QSize size )
{
	layerSizeMap[layer] = size;
	maxMinLayerSize = *layerSizeMap.begin();
	for( QMap< Layer*, QSize >::iterator i = layerSizeMap.begin() + 1; i != layerSizeMap.end(); ++i )
	{
		QSize iMin = *i;
		if( iMin.width() > maxMinLayerSize.width() )
			maxMinLayerSize.setWidth( iMin.width() );
		if( iMin.height() > maxMinLayerSize.height() )
			maxMinLayerSize.setHeight( iMin.height() );
	}
	setMinimumSize( maxMinLayerSize );
}