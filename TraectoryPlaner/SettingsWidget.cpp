#include "SettingsWidget.h"

SettingsWidget::SettingsWidget( QWidget* parent ) : QDialog( parent )
{
	ui.setupUi( this );

	ui.transectWidthSpinBox->setMinimum( 0.2 );
	ui.cacheLimitSpinBox->setMinimum( 1 );
	ui.ramLimitSpinBox->setMinimum( 10 );

	buttonColor = new QPixmap( 50, 10 );
	readSettings();
}

SettingsWidget::~SettingsWidget()
{
	delete buttonColor;
}

void SettingsWidget::tabChanged()
{
	QObject* s = sender();
	if( s == ui.TraectoryLinkButton )
		ui.stackedWidget->setCurrentIndex( 0 );
	else if( s == ui.CacheLinkButton )
		ui.stackedWidget->setCurrentIndex( 1 );
	else if( s == ui.ColorsLinkButton )
		ui.stackedWidget->setCurrentIndex( 2 );
}

void SettingsWidget::reset()
{	
	if( ui.stackedWidget->currentIndex() == 0 ) // TraectoryTab
	{
		width = 50;
		
		// update ui
		ui.transectWidthSpinBox->setValue( width );
	}
	else if( ui.stackedWidget->currentIndex() == 1 ) // CacheTab
	{
		ramLimit = 50;
		cacheLimit = 10; //10 MB
		cacheDirPath = "TileCache";
		
		// update ui
		ui.ramLimitSpinBox->setValue( ramLimit );
		ui.cacheLimitSpinBox->setValue( cacheLimit );
		ui.cachePathLineEdit->setText( cacheDirPath );

	}
	else if( ui.stackedWidget->currentIndex() == 2 ) // ColorsTab
	{
		BarrierPointColor = QColor( 255, 128, 0, 255 );
		BarrierOnHoverPointColor = QColor( 255, 255, 0, 200 );
		BarrierSelectedPointColor = QColor( 255, 255, 0, 200 );
		BarrierLineColor = QColor( 255, 128, 0, 200 );
		BarrierOnHoverLineColor = QColor( 255, 255, 0, 200 );
		BarrierSelectedLineColor = QColor( 255, 255, 255, 200 );
		

		TraectoryPointColor = QColor( 0, 255, 0, 200 );
		TraectoryOnHoverPointColor = QColor( 255, 255, 0, 200 );
		TraectorySelectedPointColor = QColor( 255, 255, 0, 200 );
		TraectoryLineColor = QColor( 0, 255, 0, 200 );
		TraectoryOnHoverLineColor = QColor( 255, 255, 0, 200 );
		TraectorySelectedLineColor = QColor( 255, 255, 255, 200 );
		

		// update ui 
		updateButtonColor( ui.barrierPointButton, BarrierPointColor );
		updateButtonColor( ui.barrierOnHoverPointButton, BarrierOnHoverPointColor );
		updateButtonColor( ui.barrierSelectedPointButton, BarrierSelectedPointColor );

		updateButtonColor( ui.barrierLineButton, BarrierLineColor );
		updateButtonColor( ui.barrierOnHoverLineButton, BarrierOnHoverLineColor );
		updateButtonColor( ui.barrierSelectedLineButton, BarrierSelectedLineColor );
		
		updateButtonColor( ui.traectoryPointButton, TraectoryPointColor );
		updateButtonColor( ui.traectoryOnHoverPointButton, TraectoryOnHoverPointColor );
		updateButtonColor( ui.traectorySelectedPointButton, TraectorySelectedPointColor ); 
		
		updateButtonColor( ui.traectoryLineButton, TraectoryLineColor );
		updateButtonColor( ui.traectoryOnHoverLineButton, TraectoryOnHoverLineColor );
		updateButtonColor( ui.traectorySelectedLineButton, TraectorySelectedLineColor );	
	}
	else return;
}

void SettingsWidget::apply()
{
	ramLimit = ui.ramLimitSpinBox->value();
	cacheLimit = ui.cacheLimitSpinBox->value();
	width = ui.transectWidthSpinBox->value();
	cacheDirPath = ui.cachePathLineEdit->text();

	writeSetting();
	this->close();
}

void SettingsWidget::writeSetting()
{
	QSettings settings( "settings.ini", QSettings::IniFormat );

//-------------------------------------------
	settings.beginGroup( "Colors" );

	settings.setValue( "BarrierPoint", BarrierPointColor );
	settings.setValue( "BarrierPointOnHover", BarrierOnHoverPointColor );
	settings.setValue( "BarrierSelectedPoint", BarrierSelectedPointColor );
	settings.setValue( "BarrierLine", BarrierLineColor );
	settings.setValue( "BarrierLineOnHover", BarrierOnHoverLineColor );
	settings.setValue( "BarrierSelectedLine", BarrierSelectedLineColor );
	
	settings.setValue( "TraectoryPoint", TraectoryPointColor );
	settings.setValue( "TraectoryPointOnHover", TraectoryOnHoverPointColor );
	settings.setValue( "TraectorySelectedPoint", TraectorySelectedPointColor );
	settings.setValue( "TraectoryLine", TraectoryLineColor );
	settings.setValue( "TraectoryLineOnHover", TraectoryOnHoverLineColor );
	settings.setValue( "TraectorySelectedLine", TraectorySelectedLineColor );
	

	settings.endGroup();
//-------------------------------------------
	settings.beginGroup( "Cache" );

	settings.setValue( "path", cacheDirPath );
	settings.setValue( "cacheLimit", cacheLimit );
	settings.setValue( "ramLimit", ramLimit );

	settings.endGroup();
//-------------------------------------------
	settings.beginGroup( "Traectory" );

	settings.setValue( "width", width );

	settings.endGroup();
//-------------------------------------------
}

void SettingsWidget::readSettings()
{
	QSettings settings( "settings.ini", QSettings::IniFormat );

	//-------------------------------------------
	settings.beginGroup( "Colors" );

	BarrierPointColor = settings.value( "BarrierPoint" ).value<QColor>();
	BarrierOnHoverPointColor = settings.value( "BarrierLineOnHover" ).value<QColor>( );
	BarrierSelectedPointColor = settings.value( "BarrierSelectedPoint" ).value<QColor>( );
	BarrierLineColor = settings.value( "BarrierLine" ).value<QColor>( );
	BarrierOnHoverLineColor = settings.value( "BarrierLineOnHover" ).value<QColor>( );
	BarrierSelectedLineColor = settings.value( "BarrierSelectedLine" ).value<QColor>( );
	
	TraectoryPointColor = settings.value( "TraectoryPoint" ).value<QColor>();
	TraectoryOnHoverPointColor = settings.value( "TraectoryPointOnHover" ).value<QColor>( );
	TraectorySelectedPointColor = settings.value( "TraectorySelectedPoint" ).value<QColor>( );
	TraectoryLineColor = settings.value( "TraectoryLine" ).value<QColor>( );
	TraectoryOnHoverLineColor = settings.value( "TraectoryLineOnHover" ).value<QColor>( );
	TraectorySelectedLineColor = settings.value( "TraectorySelectedLine" ).value<QColor>( );
	
	// update ui 
	updateButtonColor( ui.barrierPointButton, BarrierPointColor );
	updateButtonColor( ui.barrierOnHoverPointButton, BarrierOnHoverPointColor );
	updateButtonColor( ui.barrierSelectedPointButton, BarrierSelectedPointColor );
	updateButtonColor( ui.barrierLineButton, BarrierLineColor );
	updateButtonColor( ui.barrierOnHoverLineButton, BarrierOnHoverLineColor );
	updateButtonColor( ui.barrierSelectedLineButton, BarrierSelectedLineColor );

	updateButtonColor( ui.traectoryPointButton, TraectoryPointColor );
	updateButtonColor( ui.traectoryOnHoverPointButton, TraectoryOnHoverPointColor );
	updateButtonColor( ui.traectorySelectedPointButton, TraectorySelectedPointColor );
	updateButtonColor( ui.traectoryLineButton, TraectoryLineColor );
	updateButtonColor( ui.traectoryOnHoverLineButton, TraectoryOnHoverLineColor );
	updateButtonColor( ui.traectorySelectedLineButton, TraectorySelectedLineColor );

	settings.endGroup();
//-------------------------------------------
	settings.beginGroup("Cache");

	cacheDirPath = settings.value( "path" ).toString();
	cacheLimit = settings.value( "cacheLimit" ).toInt();
	ramLimit = settings.value( "ramLimit" ).toInt( );

	// update ui 
	ui.cacheLimitSpinBox->setValue( cacheLimit );
	ui.ramLimitSpinBox->setValue( ramLimit );
	ui.cachePathLineEdit->setText( cacheDirPath );

	settings.endGroup();
//-------------------------------------------
	settings.beginGroup("Traectory");
	width = settings.value( "width" ).toDouble();
	
	// update ui 
	ui.transectWidthSpinBox->setValue( width );

	settings.endGroup();
}

void SettingsWidget::setCacheDirectory()
{
	QString cacheDirPath = QFileDialog::getExistingDirectory( this, "Choose cache directory", "dir", QFileDialog::Option::ShowDirsOnly );
	if( cacheDirPath.size() )
		ui.cachePathLineEdit->setText( cacheDirPath );
}

void SettingsWidget::updateButtonColor( QPushButton* button, QColor color )
{
	if( color.isValid() )
	{
		buttonColor->fill( color );
		button->setIcon( QIcon( *buttonColor ) );
		button->setIconSize( buttonColor->rect().size() );
	}
}

void SettingsWidget::updateColor( QColor& a, QColor &b )
{
	if( b.isValid() )
		a = b;
}


void SettingsWidget::setBarrierPointColor()
{
	updateColor( BarrierPointColor, QColorDialog::getColor( BarrierPointColor, this, "Barrier point color" ) );
	updateButtonColor( ui.barrierPointButton, BarrierPointColor );
}

void SettingsWidget::setBarrierOnHoverPointColor( )
{
	updateColor( BarrierOnHoverPointColor, QColorDialog::getColor( BarrierOnHoverPointColor, this, "Barrier on hover point color" ) );
	updateButtonColor( ui.barrierOnHoverPointButton, BarrierOnHoverPointColor );
}

void SettingsWidget::setBarrierSelectedPointColor( )
{
	updateColor( BarrierSelectedPointColor, QColorDialog::getColor( BarrierSelectedPointColor, this, "Barrier selected point color" ) );
	updateButtonColor( ui.barrierSelectedPointButton, BarrierSelectedPointColor );
}

void SettingsWidget::setBarrierLineColor( )
{
	updateColor( BarrierLineColor, QColorDialog::getColor( BarrierLineColor, this, "Barrier line color" ) );
	updateButtonColor( ui.barrierLineButton, BarrierLineColor );
}

void SettingsWidget::setBarrierOnHoverLineColor( )
{
	updateColor( BarrierOnHoverLineColor, QColorDialog::getColor( BarrierOnHoverLineColor, this, "Barrier on hover line color" ) );
	updateButtonColor( ui.barrierOnHoverLineButton, BarrierOnHoverLineColor );
}

void SettingsWidget::setBarrierSelectedLineColor( )
{
	updateColor( BarrierSelectedLineColor, QColorDialog::getColor( BarrierSelectedLineColor, this, "Barrier selected line color" ) );
	updateButtonColor( ui.barrierSelectedLineButton, BarrierSelectedLineColor );
}


void SettingsWidget::setTraectoryPointColor()
{
	updateColor( TraectoryPointColor, QColorDialog::getColor( TraectoryPointColor, this, "Traectory point color" ) );
	updateButtonColor( ui.traectoryPointButton, TraectoryPointColor );
}

void SettingsWidget::setTraectoryOnHoverPointColor( )
{
	updateColor( TraectoryOnHoverPointColor, QColorDialog::getColor( TraectoryOnHoverPointColor, this, "Traectory on hover point color" ) );
	updateButtonColor( ui.traectoryOnHoverPointButton, TraectoryOnHoverPointColor );
}

void SettingsWidget::setTraectorySelectedPointColor( )
{
	updateColor( TraectorySelectedPointColor, QColorDialog::getColor( TraectorySelectedPointColor, this, "Traectory selected point color" ) );
	updateButtonColor( ui.traectorySelectedPointButton, TraectorySelectedPointColor );
}

void SettingsWidget::setTraectoryLineColor( )
{
	updateColor( TraectoryLineColor, QColorDialog::getColor( TraectoryLineColor, this, "Traectory line color" ) );
	updateButtonColor( ui.traectoryLineButton, TraectoryLineColor );
}

void SettingsWidget::setTraectoryOnHoverLineColor( )
{
	updateColor( TraectoryOnHoverLineColor, QColorDialog::getColor( TraectoryOnHoverLineColor, this, "Traectory on hover line color" ) );
	updateButtonColor( ui.traectoryOnHoverLineButton, TraectoryOnHoverLineColor );
}

void SettingsWidget::setTraectorySelectedLineColor( )
{
	updateColor( TraectorySelectedLineColor, QColorDialog::getColor( TraectorySelectedLineColor, this, "Traectory selected line color" ) );
	updateButtonColor( ui.traectorySelectedLineButton, TraectorySelectedLineColor );
}