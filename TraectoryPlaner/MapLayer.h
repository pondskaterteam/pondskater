#pragma once

#include "Layer.h"
#include "ImageManager.h"

class ImageManager;

class MapLayer : public Layer
{
public:
	MapLayer();
	~MapLayer();

	void setImageManager( ImageManager* );

	void tileDownloaded( TILE, QPixmap* );

private:
	void draw();
	void mapAdapterEvent();

	bool mouseDragEvent( MouseDragEvent* );
	bool mouseMoveEvent( MouseEvent* );
	bool wheelEvent( QWheelEvent* );	

private:
	QPoint mousePos;

	ImageManager* imageManager;

    // флаг необходимости перерисовки
    bool needRepaint;
};
