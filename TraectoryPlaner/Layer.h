#pragma once

#include "std.h"
#include "MapControl.h"

class MapControl;
class MouseEvent;
class MouseDragEvent;
class MapAdapter;

class Layer
{
	friend class MapControl;

public:
	Layer();
	~Layer();

	virtual void mapControlEvent(){}

	virtual void draw(){}
	virtual void mapAdapterEvent(){}

	virtual bool enterEvent(){ return false; }
	virtual bool leaveEvent(){ return false; }
	virtual bool mousePressEvent( MouseEvent* ){ return false; }
	virtual bool mouseReleaseEvent( MouseEvent* ){ return false; }
	virtual bool mouseMoveEvent( MouseEvent* ){ return false; }
	virtual bool mouseDragEvent( MouseDragEvent* ){ return false; }
	virtual bool wheelEvent( QWheelEvent* ){ return false; }

protected:
	void setMinSize( QSize );

private:
	void setLayerSize( QSize );

protected:
	MapControl* mapControl;
	QPixmap* pixmap;
};