#pragma once
#include <QList.h>
#include <qpoint.h>

class ConvexArea;
 class Area
{
protected:
	QList<QPointF> points;
public:
	
	Area();
	Area(QList<QPointF> v);
	Area(QVector<QPointF> v);

	Area(Area*from,QList<int> v);
	QList<QPointF> getGals(double step);
	QList<QList<int> > divByConvexAreaInt();


	QList<ConvexArea> divByConvexArea();
	QList<QPointF> getPoints();
	QList<int> getPath(int i1, int i2);

	bool isConvex(QList<int> testPoints);
	bool fullInArea(QList<int> testPoints);
	bool isConvex();
	bool isCrossing();
	bool inArea(int a, int b);
	bool crossLine(int a, int b);
	bool checkLineAndBorder(QPointF a, QPointF b, QPointF c, QPointF d);
	bool inArea(QPointF point, bool online = true);

	bool onLine(QPointF a, QPointF b, QPointF c);
	bool inArea(QList<int> testPoints);
	bool crossSubArea(QList<int> fig1, QList<int> fig2);
	
	bool checkLine(QPointF a, QPointF b, QPointF c, QPointF d);
	~Area();
};

#include "ConvexArea.h"