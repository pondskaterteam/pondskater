#include "PathModelLayer.h"

#define POINT_EPS 4.0f
#define LINE_EPS 4.0f

PathStyle defaultPathStyle;

PathStyle::PathStyle()
{
	pointColor = QColor( 255, 128, 0, 200 );
	onHoverPointColor = QColor( 255, 255, 0, 200 );
	selectedPointColor = QColor( 255, 255, 0, 200 );

	lineColor = QColor( 255, 128, 0, 200 );
	onHoverLineColor = QColor( 255, 255, 0, 200 );
	selectedLineColor = QColor( 255, 255, 255, 200 );
}

void PathStyle::setPointColor( QColor color )
{
	pointColor = color;
}

void PathStyle::setOnHoverPointColor( QColor color )
{
	onHoverPointColor = color;
}

void PathStyle::setSelectedPointColor( QColor color )
{
	selectedPointColor = color;
}

void PathStyle::setLineColor( QColor color )
{
	lineColor = color;
}

void PathStyle::setOnHoverLineColor( QColor color )
{
	onHoverLineColor = color;
}

void PathStyle::setSelectedLineColor( QColor color )
{
	selectedLineColor = color;
}

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------

double distanceToLine( QPointF a, QPointF b, QPointF c );

PathModelLayer::PathModelLayer()
{
	activePathModel = 0;
	onHoverPoint = QPair< PathModel*, uint >( 0, 0 );
	selectedPoint = QPair< PathModel*, uint >( 0, 0 );
	onHoverLine = QPair< PathModel*, uint >( 0, 0 );
	selectedLine = QPair< PathModel*, uint >( 0, 0 );
	toolBarAddMode = false;
	mouseEnter = false;
}

PathModelLayer::~PathModelLayer()
{
}

void PathModelLayer::addPathModel( PathModel* pathModel, PathStyle* pathStyle )
{
	if( pathStyle == 0 )
		pathStyle = &defaultPathStyle;
	pathStyleMap[pathModel] = pathStyle;
	QObject::connect( pathModel, SIGNAL( changed() ), this, SLOT( changed() ) );
}

void PathModelLayer::erasePathModel( PathModel* pathModel )
{
	//if( pathStyleMap.contains( pathModel ) ) //�������� �� �����
	pathStyleMap.erase( pathStyleMap.find( pathModel ) );
	if( pathModel == activePathModel )
		activePathModel = NULL;
}

void PathModelLayer::setActivePathModel( PathModel* pathModel )
{
	if( pathStyleMap.contains( pathModel ) )
		activePathModel = pathModel;
}

void PathModelLayer::selectPoint( PathModel* path, int index )
{
	if( toolBarAddMode )
		return;

	if( index == -1 )
		selectedPoint = QPair< PathModel*, uint >( 0, 0 );
	else
		selectedPoint = QPair< PathModel*, uint >( path, index );

	draw();
	mapControl->update();
}

void PathModelLayer::changed()
{
	onHoverLine = QPair< PathModel*, uint >( 0, 0 );
	selectedLine = QPair< PathModel*, uint >( 0, 0 );

	draw();
	mapControl->update();
}

void PathModelLayer::toolBarEvent( ToolBarEvent e )
{
	if( e == ToolBarAddModeOn )
	{
		toolBarAddMode = true;
		selectedPoint = QPair< PathModel*, uint >( 0, 0 );
	}
	else
		toolBarAddMode = false;

	if( e == ToolBarInsertCheckPoint && selectedLine.first )
	{
		STATE state = mapControl->getState();
		MapAdapter* mapAdapter = mapControl->getMapAdapter();
		int num = selectedLine.first->getNumberCheckPoints();

		QPoint a;
		if( selectedLine.second == 0 )
			a = mapAdapter->TransformLonLatToXY( selectedLine.first->getCheckPoint( num - 1 ), state.zoom );
		else
			a = mapAdapter->TransformLonLatToXY( selectedLine.first->getCheckPoint( selectedLine.second - 1 ), state.zoom );
		QPoint b = mapAdapter->TransformLonLatToXY( selectedLine.first->getCheckPoint( selectedLine.second ), state.zoom );
		QPoint c = a + ( b - a ) / 2;

		if( selectedLine.second == 0 )
			selectedLine.first->insertCheckPoint( num, mapAdapter->TransformXYToLonLat( c, state.zoom ) );
		else
			selectedLine.first->insertCheckPoint( selectedLine.second, mapAdapter->TransformXYToLonLat( c, state.zoom ) );
	}

	if( e == ToolBarDeleteCheckPoint && selectedPoint.first )
	{
		selectedPoint.first->eraseCheckPoint( selectedPoint.second );
		selectedPoint = QPair< PathModel*, uint >( 0, 0 );
	}

	draw();
	mapControl->update();
}

void PathModelLayer::draw()
{
	if( pathStyleMap.isEmpty() )
		return;

	pixmap->fill( QColor( 0, 0, 0, 0 ) );
	STATE state = mapControl->getState();
	MapAdapter* mapAdapter = mapControl->getMapAdapter();

	QPainter painter;
	painter.begin( pixmap );
	painter.setRenderHint( QPainter::Antialiasing, true );

	//������ ��� �����----------------------------------------------------------------------------------------------------------
	for( QMap< PathModel*, PathStyle* >::iterator i = pathStyleMap.begin(); i != pathStyleMap.end(); ++i )
	{
		int num = i.key()->getNumberCheckPoints();
		for( int i2 = 1; i2 < num; i2++ )
		{
			QPoint a = mapAdapter->TransformLonLatToXY( i.key()->getCheckPoint( i2 - 1 ), state.zoom ) - state.pos;
			QPoint b = mapAdapter->TransformLonLatToXY( i.key()->getCheckPoint( i2 ), state.zoom ) - state.pos;
			if( selectedLine.first == i.key() && selectedLine.second == i2 )
			{
				painter.setPen( QPen( i.value()->selectedLineColor, 2 ) );
				painter.setBrush( QBrush( i.value()->selectedLineColor, Qt::SolidPattern ) );
				painter.drawLine( a, b );
			}
			else if( onHoverLine.first == i.key() && onHoverLine.second == i2 )
			{
				painter.setPen( QPen( i.value()->onHoverLineColor, 2 ) );
				painter.setBrush( QBrush( i.value()->onHoverLineColor, Qt::SolidPattern ) );
				painter.drawLine( a, b );
			}
			else
			{
				painter.setPen( QPen( i.value()->lineColor, 2 ) );
				painter.setBrush( QBrush( i.value()->lineColor, Qt::SolidPattern ) );
				painter.drawLine( a, b );
			}
		}
		//������ ���������� ����� ��� ��������� �����
		if( num >= 2 && i.key()->isPathClosed() && ( !toolBarAddMode || activePathModel != i.key() ) )
		{
			QPoint a = mapAdapter->TransformLonLatToXY( i.key()->getCheckPoint( 0 ), state.zoom ) - state.pos;
			QPoint b = mapAdapter->TransformLonLatToXY( i.key()->getCheckPoint( num - 1 ), state.zoom ) - state.pos;
			if( selectedLine.first == i.key() && selectedLine.second == 0 )
			{
				painter.setPen( QPen( i.value()->selectedLineColor, 2 ) );
				painter.setBrush( QBrush( i.value()->selectedLineColor, Qt::SolidPattern ) );
				painter.drawLine( a, b );
			}
			else if( onHoverLine.first == i.key() && onHoverLine.second == 0 )
			{
				painter.setPen( QPen( i.value()->onHoverLineColor, 2 ) );
				painter.setBrush( QBrush( i.value()->onHoverLineColor, Qt::SolidPattern ) );
				painter.drawLine( a, b );
			}
			else
			{
				painter.setPen( QPen( i.value()->lineColor, 2 ) );
				painter.setBrush( QBrush( i.value()->lineColor, Qt::SolidPattern ) );
				painter.drawLine( a, b );
			}
		}
	}
	//��������� ����� � ������� ������� � ��������� ������ ��������� ����
	if( toolBarAddMode && mouseEnter && activePathModel != 0 )
	{
		painter.setPen( QPen( pathStyleMap[activePathModel]->lineColor, 2 ) );
		painter.setBrush( QBrush( pathStyleMap[activePathModel]->lineColor, Qt::SolidPattern ) );

		int num = activePathModel->getNumberCheckPoints();
		if( num )
		{
			QPoint a = mapAdapter->TransformLonLatToXY( activePathModel->getCheckPoint( num - 1 ), state.zoom ) - state.pos;
			QPoint b = mapAdapter->TransformLonLatToXY( mousePos, state.zoom ) - state.pos;
			painter.drawLine( a, b );
		}
	}

	//������ ��� �����----------------------------------------------------------------------------------------------------------
	for( QMap< PathModel*, PathStyle* >::iterator i = pathStyleMap.begin(); i != pathStyleMap.end(); ++i )
	{
		int num = i.key()->getNumberCheckPoints();
		for( int i2 = 0; i2 < num; i2++ )
		{
			QPoint a = mapAdapter->TransformLonLatToXY( i.key()->getCheckPoint( i2 ), state.zoom ) - state.pos;
			if( i.key() == selectedPoint.first && i2 == selectedPoint.second )
			{
				painter.setPen( QPen( i.value()->selectedPointColor, 2 ) );
				painter.setBrush( QBrush( i.value()->selectedPointColor, Qt::SolidPattern ) );
				painter.drawEllipse( a, 3, 3 );
			}
			else if( i.key() == onHoverPoint.first && i2 == onHoverPoint.second )
			{
				painter.setPen( QPen( i.value()->onHoverPointColor, 2 ) );
				painter.setBrush( QBrush( i.value()->onHoverPointColor, Qt::SolidPattern ) );
				painter.drawEllipse( a, 3, 3 );
			}
			else
			{
				painter.setPen( QPen( i.value()->pointColor, 2 ) );
				painter.setBrush( QBrush( i.value()->pointColor, Qt::SolidPattern ) );
				painter.drawEllipse( a, 3, 3 );
			}
		}
	}
	//������ ����� � ������� �������
	if( toolBarAddMode && mouseEnter && activePathModel != 0 )
	{
		painter.setPen( QPen( pathStyleMap[activePathModel]->pointColor, 2 ) );
		painter.setBrush( QBrush( pathStyleMap[activePathModel]->pointColor, Qt::SolidPattern ) );
		//int num = activePathModel->getNumberCheckPoints();
		QPoint a = mapAdapter->TransformLonLatToXY( mousePos, state.zoom ) - state.pos;
		painter.drawEllipse( a, 3, 3 );
	}

	painter.end();
}

void PathModelLayer::mapAdapterEvent()
{
	draw();
	mapControl->update();
}

bool PathModelLayer::enterEvent()
{
	mouseEnter = true;
	return false;
}

bool PathModelLayer::leaveEvent()
{
	mouseEnter = false;
	onHoverPoint = QPair< PathModel*, uint >( 0, 0 );
	onHoverLine = QPair< PathModel*, uint >( 0, 0 );
	draw();
	mapControl->update();
	return false;
}

bool PathModelLayer::mousePressEvent( MouseEvent* e )
{
	if( pathStyleMap.isEmpty() )
		return false;

	if( toolBarAddMode == true && e->button() == Qt::LeftButton )
	{
		//��������
		STATE state = mapControl->getState();
		activePathModel->addCheckPoint( mapControl->getMapAdapter()->TransformXYToLonLat( state.pos + e->pos(), state.zoom ) );
	}

	if( toolBarAddMode == false && e->button() == Qt::LeftButton && onHoverLine.first )
		selectedLine = onHoverLine;
	else
		selectedLine = QPair< PathModel*, uint >( 0, 0 );

	if( toolBarAddMode == false && e->button() == Qt::LeftButton )
	{
		if( selectedPoint != onHoverPoint )
			selectedPointChanged( onHoverPoint.first, onHoverPoint.second );
		selectedPoint = onHoverPoint;
	}

	draw();
	mapControl->update();
	return false;
}

bool PathModelLayer::mouseReleaseEvent( MouseEvent* )
{
	return false;
}

bool PathModelLayer::mouseMoveEvent( MouseEvent* e )
{
	STATE state = mapControl->getState();
	MapAdapter* mapAdapter = mapControl->getMapAdapter();
	mousePos = mapAdapter->TransformXYToLonLat( e->pos() + state.pos, state.zoom );

	//��������� ����� � ������� ������ ����� �����, ��� ���� ��� ��c������� ������ ���� ������ POINT_EPS
	onHoverPoint = QPair< PathModel*, uint >( 0, 0 );
	onHoverLine = QPair< PathModel*, uint >( 0, 0 );
	if( toolBarAddMode == false )
	{
		float dMinPoint = POINT_EPS + 1;
		float dMinLine = LINE_EPS + 1;
		for( QMap< PathModel*, PathStyle* >::iterator i = pathStyleMap.begin(); i != pathStyleMap.end(); ++i )
		{
			int num = i.key()->getNumberCheckPoints();
			for( int i2 = 0; i2 < num; i2++ )
			{
				QPoint a = mapAdapter->TransformLonLatToXY( i.key()->getCheckPoint( i2 ), state.zoom ) - state.pos;
				if( onHoverPoint.first == 0 )
				{
					if( i2 == 0 && i.key()->isPathClosed() )
					{
						QPoint b = mapAdapter->TransformLonLatToXY( i.key()->getCheckPoint( num - 1 ), state.zoom ) - state.pos;
						float d = distanceToLine( a, b, e->pos() );
						if( d < LINE_EPS && d < dMinLine )
						{
							dMinLine = d;
							onHoverLine.first = i.key();
							onHoverLine.second = i2;
						}
					}
					else if( i2 != 0 )
					{
						QPoint b = mapAdapter->TransformLonLatToXY( i.key()->getCheckPoint( i2 - 1 ), state.zoom ) - state.pos;
						float d = distanceToLine( a, b, e->pos() );
						if( d < LINE_EPS && d < dMinLine )
						{
							dMinLine = d;
							onHoverLine.first = i.key();
							onHoverLine.second = i2;
						}
					}
				}
				QPoint dv = a - e->pos();
				float d = sqrt( dv.x() * dv.x() + dv.y() * dv.y() );
				if( d < POINT_EPS && d < dMinPoint )
				{
					dMinPoint = d;
					onHoverPoint.first = i.key();
					onHoverPoint.second = i2;
				}
			}
		}
		if( onHoverPoint.first )
			onHoverLine = QPair< PathModel*, uint >( 0, 0 );
	}

	draw();
	mapControl->update();

	return false;
}

bool PathModelLayer::mouseDragEvent( MouseDragEvent* e )
{
	if( selectedPoint.first && e->button() == Qt::LeftButton )
	{
		STATE state = mapControl->getState();
		MapAdapter* mapAdapter = mapControl->getMapAdapter();
		QPointF newPoint = mapAdapter->TransformXYToLonLat( e->current() + state.pos, state.zoom );
		selectedPoint.first->setCheckPoint( selectedPoint.second, newPoint );

		return true;
	}

	return false;
}

double distanceToLine( QPointF a, QPointF b, QPointF c )
{
	double A = a.y() - b.y();
	double B = b.x() - a.x();
	double C = a.x() * b.y() - b.x() * a.y();
	//���������� �� � �� ������ ab
	double H1 = fabs( ( A * c.x() + B * c.y() + C ) / sqrt( A * A + B * B ) );
	double H2 = hypot( c.x() - a.x(), c.y() - a.y() );
	double H3 = hypot( c.x() - b.x(), c.y() - b.y() );

	//���������� �� ����� ������� �� ������  �� ������ �������
	double H4 = sqrt( H2 * H2 - H1 * H1 );
	double H5 = sqrt( H3 * H3 - H1 * H1 );

	double H6 = hypot( a.x() - b.x(), a.y() - b.y() );
	if( H4 + H5 - 1e-6 > H6 )
		return MIN( H2, H3 );
	else
		return H1;
}