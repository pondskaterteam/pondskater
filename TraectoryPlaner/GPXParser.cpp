#include "GPXParser.h"

GPXParser::GPXParser()
{
}

bool GPXParser::startElement( const QString			&namespaceURL, 
							  const QString			&localName, 
							  const QString			&qName, 
							  const QXmlAttributes  &attributes)
{
	for(quint16 i = 0 ; i < attributes.count() - 1 ; i++)
	{
		double x,y;
		if(attributes.localName(i) == "lat")//������
		{
			x = attributes.value(i).toDouble();
		}
		if(attributes.localName(i + 1) == "lon")//�������
		{
			y = attributes.value(i + 1).toDouble();
		}
		QPointF newCheckPoint(x, y);
		checkPoints.push_back(newCheckPoint);
	}
	return true;
}

void GPXParser::copyCheckPoints( QVector <QPointF> &points )
{
	for( quint16 i = 0 ; i < checkPoints.size() ; i++ )
		points.push_back(checkPoints[i]);
	checkPoints.clear();
}

bool GPXParser::characters(const QString& strText) 
{
    return true;
}

bool GPXParser::endElement(const QString&, const QString&, const QString& str)
{    
    return true;
}    

bool GPXParser::fatalError(const QXmlParseException &exeption)
{
	QString errorStr;
	errorStr = "Error in line:" + 
				QString::number(exeption.lineNumber()) +
				", column:" +
				QString::number(exeption.columnNumber()) +
				", Message: " +
				exeption.message();
	QMessageBox::critical(NULL, "Error", errorStr, QMessageBox::Ok);
	return false;
}