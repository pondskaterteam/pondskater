#pragma once

#include "std.h"

class ItemWidget : public QWidget
{
	friend class TableWidget;

public:
	ItemWidget( QWidget* parent = 0 );
	virtual ~ItemWidget();
		
	QObject* getObjectModel();	
	int getIndex();

	virtual void updateItem(){}
	virtual void selected( bool ){}

private:
	void childEvent( QChildEvent* );
	bool eventFilter( QObject*, QEvent* );

	void setObjectModel( QObject* objectModel );
	void setIndex( int index );

private:
	QObject* objectModel;
	int index;
};

class ItemWidgetAllocator
{
public:
	virtual ItemWidget* allocate( QWidget* parent ) = 0;
	virtual void deallocate( ItemWidget* ) = 0;
};

class TableWidget : public QWidget
{
	Q_OBJECT

	friend class ItemWidget;

public:
	TableWidget( QWidget* parent = 0 );
	~TableWidget();

	void setObjectModel( QObject* objectModel, int itemsNumber = 0 );
	void setAllocator( ItemWidgetAllocator* allocator );
	
	void setItemHeight( int height );
	void setMargins( QMargins margins );
	void setSpacing( int spacing );

signals:
	void focusChanged( int index );

public slots:
	void changed();
	void changed( int index );
	void inserted( int position, int count );
	void moved( int from, int to, int count );
	void removed( int position, int count );
	void setFocus( int index );
	void setItemsNumber( int n );

private:
	void resizeEvent( QResizeEvent* );
	void wheelEvent( QWheelEvent* );
	bool eventFilter( QObject*, QEvent* );

	void wheel( int );
	void setSelectedItem( ItemWidget* );	

	void createItemsList();
	void updateItemsWidget();

	void calcFontSize();

private slots:
	void changedScrollValue( int );

private:
	QVBoxLayout* verticalLayout;
	QHBoxLayout* horizontalLayout;
	QWidget* lineNumbersWidget;
	QWidget* nodesWidget;
	QScrollBar* verticalScroll;
	bool useScroll;
	int scrollMin, scrollMax;
	int fontSize;

	int itemHeight;
	int spacing;

	QObject* objectModel;
	ItemWidgetAllocator* allocator;
	int itemsNumber;

	QList< ItemWidget* > itemsList;
	ItemWidget* selectedItem;
	int indexSelectedItem;
};