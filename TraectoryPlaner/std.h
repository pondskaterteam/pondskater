#pragma once

#include <math.h>

#include <QtCore/QCoreApplication>
#include <QtCore/qxmlstream.h>
#include <QtCore/qsettings.h>
#include <QtCore/QThread.h>
#include <QtCore/QBuffer>
#include <QtCore/QVector>
#include <QtCore/QDebug>
#include <QtCore/QFile>
#include <QtCore/QMap>
#include <QtCore/QSet>
#include <QtCore/QDir>

#include <QtWidgets/qcolordialog.h>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/qmessagebox.h>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/qmenubar.h>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/qaction.h>
#include <QtWidgets/QWidget>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLayout>
#include <QtWidgets/qmenu.h>
#include <QtWidgets/QLabel>

#include <QtGui/qpixmapcache.h>
#include <QtGui/QPainter.h>
#include <QtGui/QEvent.h>

#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QTcpSocket.h>
#include <QtNetwork/QHostInfo>

#include <QtXml/qxml.h>


#define M_PI 3.14159265358979323846
#define MIN( a, b ) ( ( a < b ) ? a : b )
#define MAX( a, b ) ( ( a > b ) ? a : b )

typedef unsigned int uint;
typedef unsigned long long ullong;

struct TILE
{
	TILE() { zoom = x = y = 0; }
	TILE( uint zoom, uint x, uint y ) { this->zoom = zoom; this->x = x; this->y = y; }

	uint zoom;
	uint x;
	uint y;
};
	