#include "MouseEvents.h"

MouseEvent::MouseEvent( Qt::MouseButtons button, QPoint pos )
{
	b = button;
	p = pos;
}

Qt::MouseButtons MouseEvent::button()
{
	return b;
}

QPoint MouseEvent::pos()
{
	return p;
}

MouseDragEvent::MouseDragEvent( Qt::MouseButtons button, QPoint last, QPoint current )
{
	b = button;
	l = last;
	c = current;
}

Qt::MouseButtons MouseDragEvent::button()
{
	return b;
}

QPoint MouseDragEvent::last()
{
	return l;
}

QPoint MouseDragEvent::current()
{
	return c;
}

QPoint MouseDragEvent::shift()
{
	return c - l;
}