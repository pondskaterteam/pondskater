#include "PathModel.h"
#include "MapAdapter.h"
PathModel::PathModel()
{
	closed = false;
}

PathModel::~PathModel()
{
}

QVector< QPointF > PathModel::getCheckPoints( )
{
	return checkPoints;
}

void PathModel::setCheckPoints( QVector< QPointF >& checkPoints )
{
	if( this->checkPoints == checkPoints )
		return;

	int size = this->checkPoints.size();
	this->checkPoints = checkPoints;
	changed();
	removed( 0, size );
	inserted( 0, checkPoints.size() );
}

void PathModel::addCheckPoint( QPointF point )
{
	checkPoints.push_back( point );
	changed();
	inserted( checkPoints.size() - 1, 1 );
}

void PathModel::eraseCheckPoint( int index )
{
	if( checkPoints.size() > index && index >= 0 )
		checkPoints.erase( checkPoints.begin() + index );
	changed();
	removed( index, 1 );
}

void PathModel::setCheckPoint( int index, QPointF point )
{
    checkPoints[index] = point;
	changed();
	changed( index );
}

void PathModel::insertCheckPoint( int index, QPointF point )
{
	checkPoints.insert( checkPoints.begin() + index, point );
	changed();
	inserted( index, 1 );
}

void PathModel::importGPX(QString pathToFile)
{
	QFile gpxFile( pathToFile );
	//�������� ����������� ��������� ���������� ������
	if( gpxFile.open( QIODevice::ReadOnly ) )
	{
		int removedSize = checkPoints.size();
		checkPoints.clear();

		GPXParser parser;
		QXmlInputSource source( &gpxFile );
		QXmlSimpleReader reader;

		reader.setContentHandler( &parser );
		reader.parse( source );

		parser.copyCheckPoints( checkPoints );

		int insertedSize = checkPoints.size();
		if( removedSize || insertedSize )
			changed();
		if( removedSize )
			removed( 0, removedSize );
		if( insertedSize )
			inserted( 0, insertedSize );
	}
}

void PathModel::exportGPX(QString pathToFile)
{
	QFile gpxFile(pathToFile);
	if(gpxFile.open(QIODevice::WriteOnly))
	{
		QTextStream out(&gpxFile);
		out.setCodec("UTF-8");
		out << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
			<< "<gpx>\n";
		for( quint16 i = 0 ; i < checkPoints.size() ; i++ )
		{
			out << "<trkpt lat=\""
				<< QString::number(checkPoints[i].rx(), 'f', 10)
				<< "\" lon=\""
				<< QString::number(checkPoints[i].ry(), 'f', 10)
				<< "\"></trkpt>\n";
		}
		if(isPathClosed())
		{
			out << "<trkpt lat=\""
				<< QString::number(checkPoints[0].rx(), 'f', 10)
				<< "\" lon=\""
				<< QString::number(checkPoints[0].ry(), 'f', 10)
				<< "\"></trkpt>\n";
		}
		out << "</gpx>";
	}
}
bool PathModel::isCrossing()
{
	Area area(checkPoints);
	return area.isCrossing();
}
void PathModel::changeIndex( int currentIndex, int newIndex )
{
	if( currentIndex > checkPoints.size() - 1 )
		currentIndex = checkPoints.size() - 1;
	if( currentIndex < 0 )
		currentIndex = 0;
	if( newIndex > checkPoints.size( ) - 1 )
		newIndex = checkPoints.size( ) - 1;
	if( newIndex < 0 )
		newIndex = 0;
	if( currentIndex == newIndex )
		return;

    QPointF point;
    point = checkPoints[currentIndex];
	checkPoints.erase(checkPoints.begin() + currentIndex);
	checkPoints.insert(checkPoints.begin() + newIndex, point);
	changed();
	moved( currentIndex, newIndex, 1 );
}

QPointF PathModel::getCheckPoint( int index )
{
	return checkPoints[index];
}

int PathModel::getNumberCheckPoints()
{
	return checkPoints.size();
}

void PathModel::pathClose( bool isClosed)
{
	if( closed != isClosed )
	{
		closed = isClosed;
		changed( );
		changedClosureProp();		
	}
}

bool PathModel::isPathClosed()
{
	return closed;
}

void PathModel::clear()
{
	int size = checkPoints.size();
	if( size )
	{
		checkPoints.clear();
		changed();
		removed( 0, size );
	}	
}

QByteArray PathModel::saveToByteArray()
{
	QByteArray output;
	int size = checkPoints.size();
	output.append( ( char* )&size, sizeof( int ) );
	for( QVector< QPointF >::iterator i = checkPoints.begin(); i != checkPoints.end(); ++i )
	{
		QPointF point = *i;
		output.append( ( char* )&point, sizeof( QPointF ) );
	}
	output.append( ( char )closed );

	return output;
}

int PathModel::loadFromByteArray( QByteArray& byteArray, int offsetBytes )
{
	char* data = byteArray.data() + offsetBytes;
	int removedSize = checkPoints.size();
	checkPoints = QVector< QPointF >( *( int* )data, QPointF( 0.0, 0.0 ) );
	data += sizeof( int );
	for( QVector< QPointF >::iterator i = checkPoints.begin(); i != checkPoints.end(); ++i )
	{
		*i = *( QPointF* )data;
		data += sizeof( QPointF );		
	}
	int insertedSize = checkPoints.size();
	bool reg = false;
	if( closed != *( char* )data )
		reg = true;
	closed = *( char* )data;
		
	if( removedSize || insertedSize || reg )
		changed();
	if( removedSize )
		removed( 0, removedSize );
	if( insertedSize )
		inserted( 0, insertedSize );
	if( reg )
		changedClosureProp();	

	return sizeof( int ) + sizeof( QPointF ) * checkPoints.size() + sizeof( char );
}