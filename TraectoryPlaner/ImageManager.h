#pragma once

#include "std.h"
#include "MapLayer.h"
#include "MapNetwork.h"
#include "PixmapFileCache.h"

class MapLayer;

class ImageManager
{
	friend class MapLayer;

public:
	ImageManager(){ mapLayer = NULL; }
	~ImageManager(){}

	virtual void setMapAdapter( MapAdapter* ){}
	virtual QPixmap* getPixmapTile( TILE ){ return NULL; }
	
	void tileDownloaded( TILE, QPixmap* );

private:
	MapLayer* mapLayer;
};

class ImageManagerCaching : public QObject, public ImageManager
{
	Q_OBJECT

public:
	ImageManagerCaching();
	~ImageManagerCaching();

	void setMapAdapter( MapAdapter* );

	QPixmap* getPixmapTile( TILE );

	void setCacheDirPath( QString );
	void setCacheLimit( int size );
	void setRAMLimit( int size );

private slots:
	void downloaded( TILE, QPixmap* );

private:
	QString keyValue( TILE tile );

private:
	MapAdapter* mapAdapter;

    MapNetworkManaged mapNetwork;

    PixmapFileCache fileCache;

	QMap< TILE, int > tilesInProcess; // ��� ����� QSet �� � ��� ����������� ����� �������.
};
