#include "CheckpointItemWidget.h"

class MyPalette
{
public:
	MyPalette();

	QPalette palette;
} myPalete;

ItemWidget* CheckpointItemAllocator::allocate( QWidget* parent )
{
	return new CheckpointItemWidget( parent );
}

void CheckpointItemAllocator::deallocate( ItemWidget* item )
{
	delete static_cast<CheckpointItemWidget*>(item);
}

CheckpointItemWidget::CheckpointItemWidget( QWidget* parent ) : ItemWidget( parent )
{
	ui.setupUi( this );
	select = false;
}

void CheckpointItemWidget::updateItem()
{
	PathModel* path = static_cast<PathModel*>(getObjectModel());
	int index = getIndex();
	if( !path || index == -1 )
		return;

	ui.lonEdit->setText( QString( "%1" ).arg( path->getCheckPoint( index ).x(), 0, 'g', 10 ) );
	ui.latEdit->setText( QString( "%1" ).arg( path->getCheckPoint( index ).y( ), 0, 'g', 10 ) );
}

void CheckpointItemWidget::selected( bool select )
{
	if( select )
	{
 		this->select = select;
		setPalette( myPalete.palette );
		update();
	}
}

void CheckpointItemWidget::paintEvent( QPaintEvent* )
{
	QPainter painter;
	painter.begin( this );
	if( select )
		painter.fillRect( this->rect( ), myPalete.palette.color( QPalette::Active, QPalette::Button ) );
	else
		painter.fillRect( this->rect(), QBrush( QColor( 0, 0, 0, 0 ) ) );
	painter.end();
}

void CheckpointItemWidget::moveButtonClicked()
{
	PathModel* path = static_cast<PathModel*>(getObjectModel());
	path->changeIndex( getIndex(), getIndex() + ( sender() == ui.upButton ? -1 : 1 ) );
}

void CheckpointItemWidget::saveButtonClicked()
{
	PathModel* path = static_cast<PathModel*>(getObjectModel());
	path->setCheckPoint( getIndex(), QPointF( ui.lonEdit->text().toDouble(), ui.latEdit->text().toDouble() ) );
}

void CheckpointItemWidget::deleteButtonClicked()
{
	PathModel* path = static_cast<PathModel*>(getObjectModel());
	path->eraseCheckPoint( getIndex() );
}

MyPalette::MyPalette( )
{
	QBrush brush( QColor( 255, 255, 255, 255 ) );
	brush.setStyle( Qt::SolidPattern );
	palette.setBrush( QPalette::Active, QPalette::WindowText, brush );
	QBrush brush1( QColor( 22, 26, 34, 255 ) );
	brush1.setStyle( Qt::SolidPattern );
	palette.setBrush( QPalette::Active, QPalette::Button, brush1 );
	QBrush brush2( QColor( 33, 39, 51, 255 ) );
	brush2.setStyle( Qt::SolidPattern );
	palette.setBrush( QPalette::Active, QPalette::Light, brush2 );
	QBrush brush3( QColor( 27, 32, 42, 255 ) );
	brush3.setStyle( Qt::SolidPattern );
	palette.setBrush( QPalette::Active, QPalette::Midlight, brush3 );
	QBrush brush4( QColor( 11, 13, 17, 255 ) );
	brush4.setStyle( Qt::SolidPattern );
	palette.setBrush( QPalette::Active, QPalette::Dark, brush4 );
	QBrush brush5( QColor( 14, 17, 22, 255 ) );
	brush5.setStyle( Qt::SolidPattern );
	palette.setBrush( QPalette::Active, QPalette::Mid, brush5 );
	palette.setBrush( QPalette::Active, QPalette::Text, brush );
	palette.setBrush( QPalette::Active, QPalette::BrightText, brush );
	palette.setBrush( QPalette::Active, QPalette::ButtonText, brush );
	QBrush brush6( QColor( 0, 0, 0, 255 ) );
	brush6.setStyle( Qt::SolidPattern );
	palette.setBrush( QPalette::Active, QPalette::Base, brush6 );
	palette.setBrush( QPalette::Active, QPalette::Window, brush1 );
	palette.setBrush( QPalette::Active, QPalette::Shadow, brush6 );
	palette.setBrush( QPalette::Active, QPalette::AlternateBase, brush4 );
	QBrush brush7( QColor( 255, 255, 220, 255 ) );
	brush7.setStyle( Qt::SolidPattern );
	palette.setBrush( QPalette::Active, QPalette::ToolTipBase, brush7 );
	palette.setBrush( QPalette::Active, QPalette::ToolTipText, brush6 );
	palette.setBrush( QPalette::Inactive, QPalette::WindowText, brush );
	palette.setBrush( QPalette::Inactive, QPalette::Button, brush1 );
	palette.setBrush( QPalette::Inactive, QPalette::Light, brush2 );
	palette.setBrush( QPalette::Inactive, QPalette::Midlight, brush3 );
	palette.setBrush( QPalette::Inactive, QPalette::Dark, brush4 );
	palette.setBrush( QPalette::Inactive, QPalette::Mid, brush5 );
	palette.setBrush( QPalette::Inactive, QPalette::Text, brush );
	palette.setBrush( QPalette::Inactive, QPalette::BrightText, brush );
	palette.setBrush( QPalette::Inactive, QPalette::ButtonText, brush );
	palette.setBrush( QPalette::Inactive, QPalette::Base, brush6 );
	palette.setBrush( QPalette::Inactive, QPalette::Window, brush1 );
	palette.setBrush( QPalette::Inactive, QPalette::Shadow, brush6 );
	palette.setBrush( QPalette::Inactive, QPalette::AlternateBase, brush4 );
	palette.setBrush( QPalette::Inactive, QPalette::ToolTipBase, brush7 );
	palette.setBrush( QPalette::Inactive, QPalette::ToolTipText, brush6 );
	palette.setBrush( QPalette::Disabled, QPalette::WindowText, brush4 );
	palette.setBrush( QPalette::Disabled, QPalette::Button, brush1 );
	palette.setBrush( QPalette::Disabled, QPalette::Light, brush2 );
	palette.setBrush( QPalette::Disabled, QPalette::Midlight, brush3 );
	palette.setBrush( QPalette::Disabled, QPalette::Dark, brush4 );
	palette.setBrush( QPalette::Disabled, QPalette::Mid, brush5 );
	palette.setBrush( QPalette::Disabled, QPalette::Text, brush4 );
	palette.setBrush( QPalette::Disabled, QPalette::BrightText, brush );
	palette.setBrush( QPalette::Disabled, QPalette::ButtonText, brush4 );
	palette.setBrush( QPalette::Disabled, QPalette::Base, brush1 );
	palette.setBrush( QPalette::Disabled, QPalette::Window, brush1 );
	palette.setBrush( QPalette::Disabled, QPalette::Shadow, brush6 );
	palette.setBrush( QPalette::Disabled, QPalette::AlternateBase, brush1 );
	palette.setBrush( QPalette::Disabled, QPalette::ToolTipBase, brush7 );
	palette.setBrush( QPalette::Disabled, QPalette::ToolTipText, brush6 );
}