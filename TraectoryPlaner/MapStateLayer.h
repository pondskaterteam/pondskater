#pragma once

#include "std.h"
#include "Layer.h"

class MapStateLayer : public Layer
{
public:
	MapStateLayer();
	~MapStateLayer();

private:
	void mapControlEvent();

	void draw();
	void mapAdapterEvent();

	bool mouseMoveEvent( MouseEvent* );
	bool mouseDragEvent( MouseDragEvent* );

	QSize minSize();

private:
	QPoint mousePos;
};