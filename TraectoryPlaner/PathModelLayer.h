#pragma once

#include "std.h"
#include "Layer.h"
#include "PathModel.h"
#include "ToolBarWidget.h"

class PathStyle
{
	friend class PathModelLayer;

public:
	PathStyle();
	~PathStyle(){}

	void setPointColor( QColor );
	void setOnHoverPointColor( QColor );
	void setSelectedPointColor( QColor );
	void setLineColor( QColor );
	void setOnHoverLineColor( QColor );
	void setSelectedLineColor( QColor );

protected:
	QColor pointColor, onHoverPointColor, selectedPointColor;
	QColor lineColor, onHoverLineColor, selectedLineColor;
};

class PathModelLayer : public QObject, public Layer
{
	Q_OBJECT 

public:
	PathModelLayer();
	~PathModelLayer();

	void addPathModel( PathModel*, PathStyle* = 0 );
	void erasePathModel( PathModel* );
	void setActivePathModel( PathModel* = 0 );

signals:
	void selectedPointChanged( PathModel*, int index );

public slots:
	void selectPoint( PathModel*, int index );

private slots:
	void changed();
	void toolBarEvent( ToolBarEvent );

private:
	void draw();
	void mapAdapterEvent();

	bool enterEvent();
	bool leaveEvent();
	bool mousePressEvent( MouseEvent* );
	bool mouseReleaseEvent( MouseEvent* );
	bool mouseMoveEvent( MouseEvent* );
	bool mouseDragEvent( MouseDragEvent* );

private:
	QMap< PathModel*, PathStyle* > pathStyleMap;
	QPair< PathModel*, uint > onHoverPoint;
	QPair< PathModel*, uint > selectedPoint;
	QPair< PathModel*, uint > onHoverLine;
	QPair< PathModel*, uint > selectedLine;
	PathModel* activePathModel;
	bool toolBarAddMode;

	QPointF mousePos;
	bool mouseEnter;
};