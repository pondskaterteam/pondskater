#pragma once

#include "std.h"

class MapAdapter
{
public:
	MapAdapter(){}
	~MapAdapter(){}

	virtual QString GetName() = 0;

	virtual QString GetHost() = 0;
    virtual QString GetURL( uint zoom, uint x, uint y ) = 0;
	virtual QString GetRequestString( uint zoom, uint x, uint y ) = 0;

	virtual QSize GetTileSize() = 0;
	virtual QString GetTileFormat() = 0;
	virtual uint GetMaxZoom() = 0;
	virtual uint GetMinZoom() = 0;
	virtual QPoint TransformLonLatToXY( QPointF coord, uint zoom ) = 0;
	virtual QPointF TransformXYToLonLat( QPoint coord, uint zoom ) = 0;
	virtual QPointF TransformLonLatToM( QPointF coord ) = 0;
	virtual QPointF TransformMToLonLat( QPointF coord ) = 0;
};

class GoogleMapAdapter : public MapAdapter
{
public:
	GoogleMapAdapter(){}
	~GoogleMapAdapter(){}

	QString GetName();

	QString GetHost();
    QString GetURL( uint zoom, uint x, uint y );
    QString GetRequestString( uint zoom, uint x, uint y );

	QSize GetTileSize();
	QString GetTileFormat();
	uint GetMaxZoom();
	uint GetMinZoom();
	QPoint TransformLonLatToXY( QPointF coord, uint zoom );
	QPointF TransformXYToLonLat( QPoint coord, uint zoom );
	QPointF TransformLonLatToM(QPointF coord);
	QPointF TransformMToLonLat(QPointF coord);

};

class OpenStreetMapAdapter : public MapAdapter
{
public:
	OpenStreetMapAdapter(){}
	~OpenStreetMapAdapter(){}

	QString GetName();

	QString GetHost();
	QString GetURL( uint zoom, uint x, uint y );
	QString GetRequestString( uint zoom, uint x, uint y );

	QSize GetTileSize();
	QString GetTileFormat();
	uint GetMaxZoom();
	uint GetMinZoom();
	QPoint TransformLonLatToXY( QPointF coord, uint zoom );
	QPointF TransformXYToLonLat( QPoint coord, uint zoom );
	QPointF TransformLonLatToM( QPointF coord );
	QPointF TransformMToLonLat( QPointF coord );
};