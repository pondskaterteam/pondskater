#pragma once

#include "std.h"
#include "PathModel.h"
#include "ToolBarWidget.h"
#include "GenerationProgressWidget.h"
#include "MapAdapter.h"

class EditWidget : public QWidget
{
	Q_OBJECT

public:
	EditWidget( QWidget* parent = 0 );
	~EditWidget();

	uint getPathModelCount();
	PathModel* getPathModel( uint index );

signals:
	void changedPathModel( PathModel* ); // ������� ����� ��������

private:
	void blockingToolBar();

private slots:
	void clickedRollUpButton();
	void changedTab( int );
	void stateCheckBoxChanged( int );

	void changedPathClosureProp();
	void clickedGenerateTraectory();
	void clickedClearButton();

private:
	bool roolUp;
	bool disableState[2];
	int currentTab;

	PathModel barrierPolygon;
	PathModel path;

	ToolBarWidget* toolBarWidget;
};

class GenerationThread : public QThread
{
	Q_OBJECT

public:
	GenerationThread( QVector< QPointF >& barrierPolygon, MapAdapter* mapAdapter );
	~GenerationThread(){}

	void run();

	void buildPath( QVector< QPointF >& checkPoints, MapAdapter* mapAdapter );
	QVector< QPointF > getResult();

signals:
	void progress( int );

private:
	QVector< QPointF > barrierPolygon;
	MapAdapter* mapAdapter;
	QVector< QPointF > result;
};