#pragma once

#include "Area.h"

class ConvexArea :  Area{
public:
	ConvexArea();
	ConvexArea(QList<QPointF> v);
	QList<QPointF> getGals(double step);
	double square();
	double getLengthOfSide(int i);
	QPointF movePoint(int p, double step);
	~ConvexArea();
};

