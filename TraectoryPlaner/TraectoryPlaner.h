#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_TraectoryPlaner.h"
#include "MapControl.h"
#include "MapLayer.h"
#include "PathModelLayer.h"
#include "MapStateLayer.h"
#include "SettingsWidget.h"
#include "CheckpointItemWidget.h"

class BarrierStyle : public PathStyle
{
public:
	BarrierStyle();
	~BarrierStyle(){}

	void updateColors();
};

class TrajectoryStyle : public PathStyle
{
public:
	TrajectoryStyle();
	~TrajectoryStyle(){}

	void updateColors();
};

class TraectoryPlaner : public QMainWindow
{
	Q_OBJECT

public:
	TraectoryPlaner( QWidget* parent = 0 );
	~TraectoryPlaner();
	void closeEvent(QCloseEvent * CloseEvent);

	MapAdapter* getMapAdapter();

public slots:
	void changedPathModel( PathModel* );
	
	void menuSettingsTriggered();
	void menuLoadTriggered();
	void menuSaveTriggered();
	void menuSaveAsTriggered();
	
	void exportToGPX();
	void importFromGPX();

	void clickedMapTypeButtons();
	
	// ������������� ������ ����� TableWidget � PathModelLayer
public slots:
	void focusChanged( int index );
	void selectedPointChanged( PathModel*, int index );	
	//-------------------------------------------------------------------

private:
	void writeSettings();
	void readSettings();

	void resizeEvent( QResizeEvent* );

private:
	Ui::TraectoryPlanerWidget ui;
	MapLayer* mapLayer;
	ImageManagerCaching imageManager;
	PathModelLayer* pathModelLayer;
	MapStateLayer* mapStateLayer;

	BarrierStyle barrierStyle;
	TrajectoryStyle trajectoryStyle;

	GoogleMapAdapter googleMapAdapter;
	OpenStreetMapAdapter openstreetMapAdapter;

	QString currentPMFile;
	QMenu * FileMenu;
	QFileDialog * FileDialog;
	QAction * ExportToGPXAction;
	QAction * ImportFromGPXAction;

	CheckpointItemAllocator* allocator;
};
