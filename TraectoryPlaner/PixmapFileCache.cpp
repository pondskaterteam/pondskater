#include "PixmapFileCache.h"

bool operator < ( const DateTime& a, const DateTime& b )
{
	if( a.date == b.date )
		return a.time < b.time;
	else
		return a.date < b.date;
}

PixmapFileCache::PixmapFileCache()
{
	ramLimit = 50 * 1024 * 1024; // Bytes
	fileLimit = 100 * 1024 * 1024;
	ramSize = 0;
	fileSize = 0;
	correctDir = false;
}

PixmapFileCache::~PixmapFileCache()
{
	if( correctDir )
		saveCacheProperties();
}

void PixmapFileCache::setCacheDirPath( QString dirPath )
{
	if( dirPath.isEmpty() )
		dirPath = "TileCache";
	
	if( cachePath == dirPath )
		return;

	if( !QDir( dirPath ).isReadable() )
		QDir().mkpath( dirPath );

	if( correctDir )
	{
		saveCacheProperties();
		clear();
	}

	correctDir = true;
	cachePath = dirPath;

	QFile file( dirPath + QDir().separator() + "tileCache.tc" );
	if( file.exists() )
	{
		file.close();
		loadCacheProperties();
	}
}

void PixmapFileCache::setCacheLimit( int size )
{
	fileLimit = ( long long )size * 1024 * 1024;
}

void PixmapFileCache::setRamLimit( int size )
{
	ramLimit = ( long long )size * 1024 * 1024;
}

long long PixmapFileCache::cacheLimit()
{
	return fileLimit;
}

bool PixmapFileCache::insert( const QString& key, QPixmap* pixmap )
{
	if( tilePropertiesMap.contains( key ) || pixmap == 0 )
		return false;

	QByteArray bytes;
	QBuffer buffer( &bytes );
	buffer.open( QIODevice::WriteOnly );
	pixmap->save( &buffer, key.split( '.' ).back().toUtf8() );
	delete pixmap;

	TileProperties prop;
	fileSize += prop.size = bytes.size();
	prop.date = QDate::currentDate();
	prop.time = QTime::currentTime();

	tilePropertiesMap[key] = prop;

	if( fileSize > fileLimit )
		freeFileCache();

	QFile file( cachePath + QDir().separator() + key );
	file.open( QIODevice::WriteOnly );
	file.write( bytes );
	file.close();

	return true;
}

bool PixmapFileCache::remove( const QString &key )
{
	if( !tilePropertiesMap.contains( key ) )
		return false;

	QFile file( cachePath + QDir().separator() + key );
	file.remove();
	file.close();

	if( pixmapTileMap.contains( key ) )
		pixmapTileMap.erase( pixmapTileMap.find( key ) );
	tilePropertiesMap.erase( tilePropertiesMap.find( key ) );

	return true;
}

QPixmap* PixmapFileCache::load( const QString& key )
{
	if( pixmapTileMap.contains( key ) )
	{
		tilePropertiesMap[key].date = QDate::currentDate();
		tilePropertiesMap[key].time = QTime::currentTime();
		return pixmapTileMap[key];
	}

	if( tilePropertiesMap.contains( key ) )
	{
		QPixmap* pixmap = new QPixmap( cachePath + QDir().separator() + key, key.split( '.' ).back().toUtf8() );
		ramSize += pixmap->depth() * pixmap->width() * pixmap->height() / 8;
		if( ramSize > ramLimit )
			freeRamCache();
		
		pixmapTileMap[key] = pixmap;
		tilePropertiesMap[key].date = QDate::currentDate();
		tilePropertiesMap[key].time = QTime::currentTime();
		return pixmap;
	}

	return 0;
}

void PixmapFileCache::clear()
{
	for( QMap< QString, QPixmap* >::iterator i = pixmapTileMap.begin(); i != pixmapTileMap.end(); ++i )
		delete i.value();
	pixmapTileMap.clear();
	tilePropertiesMap.clear();
	ramSize = 0;
	fileSize = 0;
}

void PixmapFileCache::loadCacheProperties()
{
	clear();

	QFile file( cachePath + QDir().separator() + "tileCache.tc" );
	file.open( QIODevice::ReadOnly );
	while( true )
	{
		QString line = file.readLine();
		if( line.isEmpty() )
			break;
		QStringList stringList = line.split( '$' );
		tilePropertiesMap[stringList[0]] = TileProperties( stringList[1].toInt(), QDate::fromString( stringList[2] ), QTime::fromString( stringList[3] ) );
		fileSize += stringList[1].toInt();
	}
}

void PixmapFileCache::saveCacheProperties()
{
	QFile file( cachePath + QDir().separator() + "tileCache.tc" );
	file.open( QIODevice::WriteOnly );
	for( QMap< QString, TileProperties >::iterator i = tilePropertiesMap.begin(); i != tilePropertiesMap.end(); ++i )
	{
		file.write( i.key().toUtf8() );
		file.write( "$" );
		file.write( QString( "%1" ).arg( i.value().size ).toUtf8() );
		file.write( "$" );
		file.write( i.value().date.toString().toUtf8() );
		file.write( "$" );
		file.write( i.value().time.toString().toUtf8() );
		file.write( "\n" );
	}
}

void PixmapFileCache::freeRamCache() // ������� �� 30%
{
	QMultiMap< DateTime, QString > timeRamCacheMap;
	for( QMap< QString, QPixmap* >::iterator i = pixmapTileMap.begin(); i != pixmapTileMap.end(); ++i )
		timeRamCacheMap.insert( DateTime( tilePropertiesMap[i.key()].date, tilePropertiesMap[i.key()].time ), i.key() );

	long long newSize = ramSize * 0.7;
	for( QMultiMap< DateTime, QString >::iterator i = timeRamCacheMap.begin(); i != timeRamCacheMap.end(); ++i )
	{
		QPixmap* pixmap = pixmapTileMap[i.value()];
		ramSize -= pixmap->depth() * pixmap->width() * pixmap->height() / 8;
		delete pixmap;
		pixmapTileMap.erase( pixmapTileMap.find( i.value() ) );

		if( ramSize <= newSize )
			break;
	}
}

void PixmapFileCache::freeFileCache() // ������� �� 30%
{
	QMultiMap< DateTime, QString > timeFileCacheMap;
	for( QMap< QString, TileProperties >::iterator i = tilePropertiesMap.begin(); i != tilePropertiesMap.end(); ++i )
		timeFileCacheMap.insert( DateTime( i.value().date, i.value().time ), i.key() );

	long long newSize = fileSize * 0.7;
	for( QMultiMap< DateTime, QString >::iterator i = timeFileCacheMap.begin(); i != timeFileCacheMap.end(); ++i )
	{
		fileSize -= tilePropertiesMap[i.value()].size;
		if( pixmapTileMap.contains( i.value() ) )
		{
			QPixmap* pixmap = pixmapTileMap[i.value()];
			ramSize -= pixmap->depth() * pixmap->width() * pixmap->height() / 8;
			delete pixmap;
			pixmapTileMap.erase( pixmapTileMap.find( i.value() ) );
		}
		QFile file( cachePath + QDir().separator() + i.value() );
		file.remove();
		file.close();
		tilePropertiesMap.erase( tilePropertiesMap.find( i.value() ) );

		if( fileSize <= newSize )
			break;
	}
}

PixmapFileCache::TileProperties::TileProperties()
{
	size = 0;
}

PixmapFileCache::TileProperties::TileProperties( long long size, QDate date, QTime time )
{
	this->size = size;
	this->date = date;
	this->time = time;
}

DateTime::DateTime( QDate date, QTime time )
{
	this->date = date;
	this->time = time;
}