#include "GenerationProgressWidget.h"

GenerationProgressWidget::GenerationProgressWidget( QWidget* parent ) : QDialog( parent )
{
	ui.setupUi( this );
}

GenerationProgressWidget::~GenerationProgressWidget()
{

}

void GenerationProgressWidget::progress( int a )
{
	ui.progressBar->setValue( a );
	if( a == 100 )
	{
		buildCompleted = true;
		close();
	}
	else
		buildCompleted = false;
}

bool GenerationProgressWidget::buildIsCompleted()
{
	return buildCompleted;
}