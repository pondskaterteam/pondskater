#include "TableWidget.h"

TableWidget::TableWidget( QWidget* parent ) : QWidget( parent )
{
	useScroll = false;
	scrollMin = scrollMax = 0;

	itemHeight = 20;
	spacing = 5;

	calcFontSize();

	objectModel = NULL;
	allocator = NULL;
	itemsNumber = 0;

	selectedItem = NULL;
	indexSelectedItem = -1;

	verticalLayout = new QVBoxLayout( this );
	horizontalLayout = new QHBoxLayout();
	lineNumbersWidget = new QWidget();
	nodesWidget = new QWidget();
	QFrame* vLine = new QFrame();
	QFrame* hLine = new QFrame();
	verticalScroll = new QScrollBar();

	verticalLayout->setObjectName( "tableVL" );
	horizontalLayout->setObjectName( "tableHL" );
	lineNumbersWidget->setObjectName( "lineNumbersWidget" );
	vLine->setObjectName( "vLine" );
	hLine->setObjectName( "hLine" );
	verticalScroll->setObjectName( "verticalScroll" );

	this->setMargins( QMargins( 0, 0, 0, 0 ) );

	vLine->setFrameShape( QFrame::VLine );
	vLine->setFrameShadow( QFrame::Sunken );
	hLine->setFrameShape( QFrame::HLine );
	hLine->setFrameShadow( QFrame::Sunken );

	lineNumbersWidget->setFixedWidth( 100 );

	verticalLayout->setContentsMargins( QMargins( 0, 0, 0, 0 ) );
	verticalLayout->setSpacing( 0 );
	verticalLayout->addWidget( hLine );
	verticalLayout->addLayout( horizontalLayout );

	horizontalLayout->setContentsMargins( QMargins( 0, 0, 0, 0 ) );
	horizontalLayout->setSpacing( 0 );
	horizontalLayout->addWidget( lineNumbersWidget );
	horizontalLayout->addWidget( vLine );
	horizontalLayout->addWidget( nodesWidget );

	QObject::connect( verticalScroll, SIGNAL( valueChanged( int ) ), this, SLOT( changedScrollValue( int ) ) );
	lineNumbersWidget->installEventFilter( this );
	nodesWidget->installEventFilter( this );
}

TableWidget::~TableWidget()
{
	for( QList< ItemWidget* >::iterator i = itemsList.begin(); i != itemsList.end(); ++i )
		allocator->deallocate( *i );
	itemsList.clear();
	if( selectedItem )
	{
		allocator->deallocate( selectedItem );
		selectedItem = NULL;
		indexSelectedItem = -1;
	}

	objectModel = NULL;
	allocator = NULL;
}

void TableWidget::setObjectModel( QObject* objectModel, int itemsNumber )
{
	this->objectModel = objectModel;
	this->itemsNumber = itemsNumber;
	for( QList< ItemWidget* >::iterator i = itemsList.begin(); i != itemsList.end(); ++i )
		this->allocator->deallocate( *i );
	itemsList.clear();
	if( selectedItem )
	{
		allocator->deallocate( selectedItem );
		selectedItem = NULL;
		indexSelectedItem = -1;
	}
	createItemsList();
}

void TableWidget::setAllocator( ItemWidgetAllocator* allocator )
{
	for( QList< ItemWidget* >::iterator i = itemsList.begin(); i != itemsList.end(); ++i )
		this->allocator->deallocate( *i );
	itemsList.clear();
	if( selectedItem )
	{
		this->allocator->deallocate( selectedItem );
		selectedItem = NULL;
		indexSelectedItem = -1;
	}

	this->allocator = allocator;
	createItemsList();
}

void TableWidget::setItemHeight( int height )
{
	if( itemHeight != height )
	{
		itemHeight = height;
		calcFontSize();
		createItemsList();
	}
}

void TableWidget::setMargins( QMargins margins )
{
	// = margins;
}

void TableWidget::setSpacing( int spacing )
{
	spacing += 2; // ��� ����������� �����
	if( this->spacing != spacing )
	{
		this->spacing = spacing;
		calcFontSize();
		createItemsList();
	}
}

void TableWidget::changed()
{
	if( selectedItem )
	{
		this->allocator->deallocate( selectedItem );
		selectedItem = NULL;
		indexSelectedItem = -1;
	}
	updateItemsWidget();
}

void TableWidget::changed( int index )
{
	updateItemsWidget();
}

void TableWidget::inserted( int position, int count )
{
	if( selectedItem )
	{
		if( indexSelectedItem >= position )
		{
			selectedItem->setIndex( indexSelectedItem + count );
			indexSelectedItem += count;
			selectedItem->updateItem();
		}
	}
	itemsNumber += count;
	createItemsList();
}

void TableWidget::moved( int from, int to, int count )
{
	if( selectedItem && indexSelectedItem >= from && indexSelectedItem < from + count )
	{
		indexSelectedItem += to - from;
		selectedItem->setIndex( indexSelectedItem );
	}

	updateItemsWidget();
}

void TableWidget::removed( int position, int count )
{
	bool reg = false;
	if( selectedItem )
	{
		if( indexSelectedItem >= position && indexSelectedItem < position + count )
		{
			this->allocator->deallocate( selectedItem );
			selectedItem = NULL;
			indexSelectedItem = -1;
			focusChanged( -1 );
			reg = true;
		}
		else if( indexSelectedItem >= position ) // ��� ��� ������, ������� ���������������
		{
			selectedItem->setIndex( indexSelectedItem - count );
			indexSelectedItem -= count;
			selectedItem->updateItem();
		}
	}
	itemsNumber -= count;
	createItemsList();
	if( reg )
		updateItemsWidget();
}

void TableWidget::setFocus( int index )
{
	if( indexSelectedItem == index )
		return;

	if( selectedItem )
	{
		selectedItem->setIndex( index );
		indexSelectedItem = index;
	}
	else
	{
		selectedItem = allocator->allocate( nodesWidget );
		selectedItem->setObjectModel( objectModel );
		selectedItem->setIndex( index );
		selectedItem->selected( true );
		indexSelectedItem = index;
	}

	if( useScroll )
		verticalScroll->setValue( index * ( itemHeight + spacing ) - scrollMin - 2 );
	updateItemsWidget( );	
}

void TableWidget::setItemsNumber( int n )
{
	if( itemsNumber != n )
	{
		itemsNumber = n;
		if( selectedItem )
		{
			allocator->deallocate( selectedItem );
			selectedItem = NULL;
			indexSelectedItem = -1;
		}
		createItemsList();
	}
}

void TableWidget::resizeEvent( QResizeEvent* e )
{
	createItemsList();
	updateItemsWidget();
}

void TableWidget::wheelEvent( QWheelEvent* e )
{
	if( useScroll )
		verticalScroll->setValue( verticalScroll->value() + ( e->delta() > 0 ? -1 : 1 ) * ( itemHeight + spacing ) );
}

bool TableWidget::eventFilter( QObject* obj, QEvent* e )
{
	if( e->type() == QEvent::Paint )
	{
		//������ ����� ����� ���������
		QColor dark = palette().dark().color();
		QColor light = palette().light().color();
		QColor textColor = palette().text().color();
		QPainter painter;
		painter.begin( ( QWidget* )obj );
		painter.setFont( QFont( "Calibri", fontSize ) );
		int offset = -2; // ������ ��� ����???  ����� ��-�� �������������� �����?--------------------------------------------------------------------------
		int startIndex = 1;
		if( useScroll )
		{
			offset = -( verticalScroll->value() % ( itemHeight + spacing ) ) + itemHeight + spacing / 2 - 1;
			startIndex = verticalScroll->value() / ( itemHeight + spacing ) + 1;
		}
		int width = ( ( QWidget* )obj )->width();
		int height = ( ( QWidget* )obj )->height();
		while( offset < height + itemHeight + spacing )
		{
			painter.setPen( QPen( dark ) );
			painter.drawLine( QPoint( 0, offset ), QPoint( width, offset ) );
			painter.setPen( QPen( light ) );
			painter.drawLine( QPoint( 0, offset + 1 ), QPoint( width, offset + 1 ) );

			//������ ������ �����
			if( obj == lineNumbersWidget )
			{
				painter.setPen( QPen( textColor ) );
				painter.drawText( QRect( 0, offset - ( useScroll ? ( itemHeight + spacing ) : 0 ), width, itemHeight + spacing ), QString( "%1" ).arg( startIndex++ ), QTextOption( Qt::AlignHCenter | Qt::AlignVCenter ) );
			}

			offset += itemHeight + spacing;
		}
		painter.end();

		return true;
	}
	else if( e->type() == QEvent::Resize && obj == nodesWidget )
	{
		updateItemsWidget();
	}

	return QWidget::eventFilter( obj, e );
}

void TableWidget::wheel( int delta )
{
	if( useScroll )
		verticalScroll->setValue( verticalScroll->value() + ( delta > 0 ? -1 : 1 ) * ( itemHeight + spacing ) );
}

void TableWidget::setSelectedItem( ItemWidget* item )
{
	if( selectedItem == item )
		return;

	bool reg = false;
	if( selectedItem )
	{
		selectedItem->selected( false );
		allocator->deallocate( selectedItem );
		selectedItem = NULL;
		indexSelectedItem = -1;
		reg = true;
	}
	for( QList< ItemWidget* >::iterator i = itemsList.begin(); i != itemsList.end(); ++i )
	{
		if( *i == item )
		{
			selectedItem = item;
			indexSelectedItem = item->getIndex();
			selectedItem->selected( true );

			ItemWidget* newItem = allocator->allocate( nodesWidget );
			newItem->setObjectModel( objectModel );
			newItem->hide();
			*i = newItem;
			break;
		}
	}
	if( reg )
		updateItemsWidget();

	focusChanged( indexSelectedItem);
}

void TableWidget::createItemsList()
{
	if( allocator == NULL )
		return;

	int n = 0;
	if( itemsNumber )
	{
		if( ( itemsNumber * itemHeight + ( itemsNumber - 1 ) * spacing + ( spacing / 2 - 1 ) * 2 ) <= nodesWidget->height() )
		{
			n = itemsNumber;
			if( useScroll )
			{
				horizontalLayout->removeWidget( verticalScroll );
				verticalScroll->hide();
			}
			useScroll = false;
		}
		else
		{
			n = nodesWidget->height() / ( itemHeight + spacing );
			int mod = nodesWidget->height() % ( itemHeight + spacing );
			if( mod > spacing )
				n += 2;
			else
				n++;
			if( !useScroll )
			{
				horizontalLayout->addWidget( verticalScroll );
				verticalScroll->show();
			}
			useScroll = true;

			double scrollProgress = verticalScroll->value() / ( double )( abs( scrollMax ) - abs( scrollMin ) );
			scrollMin = -spacing / 2 + 1;
			scrollMax = itemsNumber * itemHeight + ( itemsNumber - 1 ) * spacing - nodesWidget->height() + spacing / 2 - 1;
			verticalScroll->setRange( scrollMin, scrollMax );
			verticalScroll->setValue( ( abs( scrollMax ) - abs( scrollMin ) ) * scrollProgress + scrollMin );
		}
	}
	else
	{
		if( useScroll )
		{
			horizontalLayout->removeWidget( verticalScroll );
			verticalScroll->hide();
		}
		useScroll = false;
	}

	if( n > itemsList.size() )
	{
		int delta = n - itemsList.size();
		for( int i = 0; i < delta; i++ )
		{
			ItemWidget* item = allocator->allocate( nodesWidget );
			//item->setParent( nodesWidget ); // ��� ���� ����� �� ������, �� ���������� ��� �� �������� ���� ��� ����� ��������� ���������� �����, ���-�� ��������...
			item->setObjectModel( objectModel );
			itemsList.append( item );
		}
		updateItemsWidget();
	}
	if( n < itemsList.size() )
	{
		int delta = itemsList.size() - n;
		for( int i = 0; i < delta; i++ )
		{
			allocator->deallocate( itemsList.back() );
			itemsList.pop_back();
		}
		updateItemsWidget();
	}
}

void TableWidget::updateItemsWidget()
{
	int itemsWidgetWidth = nodesWidget->width();
	if( useScroll )
	{
		int offset = verticalScroll->value() % ( itemHeight + spacing );
		int indexBegin = verticalScroll->value() / ( itemHeight + spacing );
		int counter = 0;
		bool selectedItemVisibleNow = false;
		for( QList< ItemWidget* >::iterator i = itemsList.begin(); i != itemsList.end(); ++i, ++counter )
		{
			if( indexBegin + counter < itemsNumber )
			{
				if( indexBegin + counter == indexSelectedItem )
				{
					selectedItem->show();
					selectedItem->setGeometry( 0, counter * ( itemHeight + spacing ) - offset, itemsWidgetWidth, itemHeight );
					selectedItem->updateItem();
					( *i )->hide();
					( *i )->setIndex( -1 );
					selectedItemVisibleNow = true;
				}
				else
				{
					( *i )->show();
					( *i )->setGeometry( 0, counter * ( itemHeight + spacing ) - offset, itemsWidgetWidth, itemHeight );
					( *i )->setIndex( indexBegin + counter );
				}
			}
			else
			{
				( *i )->hide();
				( *i )->setIndex( -1 );
			}
			( *i )->updateItem();
		}
		if( selectedItem && !selectedItemVisibleNow )
			selectedItem->setGeometry( 0, -2 * itemHeight, itemsWidgetWidth, itemHeight );
	}
	else
	{
		int index = 0;
		for( QList< ItemWidget* >::iterator i = itemsList.begin(); i != itemsList.end(); ++i, ++index )
		{
			if( index == indexSelectedItem )
			{
				selectedItem->show();
				selectedItem->setGeometry( 0, index * ( itemHeight + spacing ) + spacing / 2 - 1, itemsWidgetWidth, itemHeight );
				selectedItem->updateItem();
				( *i )->hide();
				( *i )->setIndex( -1 );
			}
			else
			{
				( *i )->show();
				( *i )->setGeometry( 0, index * ( itemHeight + spacing ) + spacing / 2 - 1, itemsWidgetWidth, itemHeight );
				( *i )->setIndex( index );
			}
			( *i )->updateItem();
		}
	}

	//����������� ������ ������� ������� �����
	QFontMetrics metrics( QFont( "Calibri", fontSize ) );
	int maxIndex;
	if( useScroll )
		maxIndex = ( verticalScroll->value() + nodesWidget->height() + 4 ) / ( itemHeight + spacing );
	else
		maxIndex = itemsNumber - 1;
	lineNumbersWidget->setFixedWidth( metrics.width( QString( "%1" ).arg( maxIndex + 1 ) ) + 5 );

	lineNumbersWidget->update();
	nodesWidget->update();
}

void TableWidget::calcFontSize()
{
	int minSize = 12;
	int maxSize = 36;
	fontSize = minSize;

	for( int i = minSize + 1; i <= maxSize; i++ )
	{
		QFontMetrics metrics( QFont( "Calibri", i ) );
		if( itemHeight + spacing - 2 > metrics.height() )
			fontSize = i;
		else break;
	}
}

void TableWidget::changedScrollValue( int v )
{
	updateItemsWidget();
}

ItemWidget::ItemWidget( QWidget* parent ) : QWidget( parent )
{
	objectModel = NULL;
	index = -1;
}

ItemWidget::~ItemWidget()
{
	objectModel = NULL;
	index = -1;
}

void ItemWidget::setObjectModel( QObject* objectModel )
{
	this->objectModel = objectModel;
}

QObject* ItemWidget::getObjectModel()
{
	return objectModel;
}

void ItemWidget::setIndex( int index )
{
	this->index = index;
}

int ItemWidget::getIndex()
{
	return index;
}

void ItemWidget::childEvent( QChildEvent * e )
{
	if( e->type() == QEvent::ChildAdded )
		e->child()->installEventFilter( this );
	else if( e->type() == QEvent::ChildRemoved )
		e->child()->removeEventFilter( this );
}

bool ItemWidget::eventFilter( QObject* object, QEvent* e )
{
	if( e->type() == QEvent::FocusIn )
		( static_cast< TableWidget* >( parent()->parent() ) )->setSelectedItem( this );
	if( e->type() == QEvent::Wheel )
		( static_cast< TableWidget* >( parent()->parent() ) )->wheel( ( ( QWheelEvent* ) e )->delta() );

	return false;
	//return QWidget::eventFilter( object, e );
}