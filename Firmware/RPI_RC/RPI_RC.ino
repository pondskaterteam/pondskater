#include <Servo.h> 
#include <Wire.h>

const int ledPin = 13;

const int driveOut = 6;
const int servoOut = 7;

const int driveIn = 8;
const int servoIn = 9;
const int switchIn = 10;

int durationDrive;
int durationServo;
int durationSwitch;

const int NEUTRAL_TURN = 1500; // this is the duration in microseconds of neutral turn on a RC
const int FILTER_TURN = 50;

Servo drive;
Servo servo;

const int SLAVE_ADDRESS 	= 0x04;

const int SIGNAL_BAD		= 0;
const int SIGNAL_GOOD		= 1;

volatile byte battVolts = 0;
volatile byte RC_Signal = SIGNAL_BAD;
volatile byte RC_Switch = 0;
volatile byte RC_Drive = 0;
volatile byte RC_Servo = 0;

volatile byte regServo = 90;
volatile byte regDrive = 2;

volatile byte regNum = 0;

const int REG_RC_Signal = 		190; // durationSwitch
const int REG_RC_Switch = 		191; // durationSwitch
const int REG_RC_Drive = 		192; // durationDrive
const int REG_RC_Servo = 		193; // durationServo
const int REG_BattVolts =		194; // battVolts
const int REG_Throttle = 		195; // direct to Servo drive
const int REG_Servo = 			196; // direct to Servo servo

// the setup routine runs once when you press reset:
void setup() {         
       
	// initialize the digital pin as an output.
	pinMode(ledPin, OUTPUT); 

	drive.attach(driveOut);
	drive.write(2);
	
	servo.attach(servoOut);
	servo.write(90);
	
	pinMode(driveIn, INPUT);
	pinMode(servoIn, INPUT);
	pinMode(switchIn, INPUT);
	
    // initialize i2c as slave
    Wire.begin(SLAVE_ADDRESS);
	
	// define callbacks for i2c communication
    Wire.onReceive(receiveData);
    Wire.onRequest(sendData);
	
	// Init end
	Blink();
	
	
}

// the loop routine runs over and over again forever:
void loop() {
	
	// Read battery voltage
	battVolts = analogRead(0) >> 2;
	
	durationSwitch = pulseIn(switchIn, HIGH, 20000);
	
	if (durationSwitch < 600) // No signal from "pulseIn(switchIn, HIGH, 20000);" 
	{
		RC_Signal = SIGNAL_BAD;
		
		servo.write(90); // Center motor shaft
		drive.write(0); // Stop drive
		
		// indicate error
		Blink(); 
		delay(250);
	}
	else
	{
		RC_Signal = SIGNAL_GOOD;
		
		RC_Switch = mapPPM2Byte(durationSwitch);
		
		durationDrive = pulseIn(driveIn, HIGH, 20000);
		RC_Drive = mapPPM2Byte(durationDrive);
		
		durationServo = pulseIn(servoIn, HIGH, 20000);
		RC_Servo = mapPPM2Byte(durationServo);
			
		if (durationSwitch < 1200) // Hard manual mode
		{
			digitalWrite(ledPin, HIGH);
			
			// filter noise in zero pos
			if (durationServo > NEUTRAL_TURN-FILTER_TURN && durationServo < NEUTRAL_TURN+FILTER_TURN)
			{
				servo.write(90);
			}
			else
			{
				servo.writeMicroseconds(durationServo);
			}
			
			drive.writeMicroseconds(durationDrive);
		}
		else // Drive by Autopilot
		{
			digitalWrite(ledPin, LOW);
			drive.write(regDrive);
			servo.write(regServo);
		}
	}
  
}

byte mapPPM2Byte(int value)
{
	int val = constrain(value, 1000, 2000);
	return map(val, 1000, 2000, 0, 180);
}

// callback for received data
void receiveData(int byteCount){
	
	while (Wire.available())
	{
		int val = Wire.read();
		if (val > 180)
		{
			regNum = val;
		}
		else
		{
			if(REG_Throttle == regNum)
			{
				regDrive = val;
			}
			
			if(REG_Servo == regNum)
			{
				regServo = val;
			}
			
		}
	}
}

// callback for sending data
void sendData(){
	switch (regNum)
	{
		case REG_RC_Signal:
			Wire.write(RC_Signal);
		break;
		case REG_RC_Switch:
			Wire.write(RC_Switch);
		break;
		case REG_RC_Drive:
			Wire.write(RC_Drive);
		break;
		case REG_RC_Servo:
			Wire.write(RC_Servo);
		break;
		case REG_BattVolts:
			Wire.write(battVolts);
		break;
	}
}

void Blink()
{
	digitalWrite(ledPin, HIGH);   // turn the LED on (HIGH is the voltage level)
	delay(250);               // wait for a second
	digitalWrite(ledPin, LOW);    // turn the LED off by making the voltage LOW
}

