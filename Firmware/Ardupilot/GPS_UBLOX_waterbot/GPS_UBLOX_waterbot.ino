#include <stdlib.h>
#include <AP_Common.h>
#include <AP_Progmem.h>
#include <AP_Param.h>
#include <AP_HAL.h>
#include <AP_HAL_AVR.h>
#include <AP_HAL_Empty.h>
#include <AP_HAL_PX4.h>
#include <AP_GPS.h>
#include <DataFlash.h>
#include <AP_InertialSensor.h>
#include <AP_ADC.h>
#include <GCS_MAVLink.h>
#include <AP_Baro.h>
#include <Filter.h>
#include <AP_AHRS.h>
#include <AP_Compass.h>
#include <AP_Declination.h>
#include <AP_Airspeed.h>
#include <AP_Vehicle.h>
#include <AP_ADC_AnalogSource.h>
#include <AP_Mission.h>
#include <AP_Math.h>
#include <AP_Notify.h>

const AP_HAL::HAL& hal = AP_HAL_BOARD_DRIVER;

#define BUFFER_SIZE 100
char buffer[BUFFER_SIZE];
int buffer_counter;

void cleanBuffer(int forSymbol = BUFFER_SIZE) {
    if (forSymbol < 0 || forSymbol > BUFFER_SIZE) {
        forSymbol = BUFFER_SIZE;
    }
    for (int i = 0; i < forSymbol; i++) {
        buffer[i] = '\0';
    } 
    buffer_counter = 0;
}

void setup()
{
    // initialise console uart to 115200 baud
    hal.console->begin(115200);
    
    // initialise bluetooth uart to 38400 baud
    hal.uartC->begin(38400);

    // initialise gps uart to 38400 baud
    hal.uartB->begin(38400);
    
    cleanBuffer();
}

bool isConnected = false;
bool isThirteen = false;
bool isTen = false;
bool isFind1310 = false;
bool isFind1310Thirteen = false;
char disconnected_string[] = "DISCONNECT  '              '";
int disconnected_counter = 0;

void loop()
{
    char symbol = 0;
        
    if (!isConnected) {
          
          while ( symbol != 'C' ) {
              if (hal.uartC->available()) {
                  symbol = hal.uartC->read();
              }
              if( hal.uartB->available() ) {
                  hal.uartB->flush();
              }
          }
          hal.console->write("Catch C.");
          while ( symbol != 10 ) {
              if (hal.uartC->available()) {
                  symbol = hal.uartC->read();
              }
              if( hal.uartB->available() ) {
                  hal.uartB->flush();
              }
          }
          hal.console->write("Catch 10.");          
          isConnected = true;
          hal.console->write("State changed. True.");
      
              
    } else {
        
        while (hal.uartB->available()) {
            hal.uartC->write(hal.uartB->read());            
        }  



        if (hal.uartC->available()) {
            symbol = hal.uartC->read();

            if ( isFind1310Thirteen ) {
                if ( symbol == 10 ) {
                    hal.console->write("#10.");
                    isConnected = false;
                    hal.console->write("State changed. False.");                    
                }
                else {
                    hal.uartB->write(13);
                    hal.uartB->write(10);
                    for (int i = 0; i < disconnected_counter; i++) {
                        hal.uartB->write(disconnected_string[i]);
                    }
                    hal.uartB->write(13);
                    isTen = false;
                    isThirteen = false;
                    isFind1310 = false;
                    isFind1310Thirteen = false;
                    disconnected_counter = 0;
                    hal.console->write("isFind1310Thirteen false.");    
                }
            } else if ( isFind1310 ) {
                if ( symbol == 13 ) {
                    isFind1310Thirteen = true;
                    hal.console->write("#13.");
                } else {
                    hal.uartB->write(13);
                    hal.uartB->write(10);
                    for (int i = 0; i < disconnected_counter; i++) {
                        hal.uartB->write(disconnected_string[i]);
                    }
                    isTen = false;
                    isThirteen = false;
                    isFind1310 = false;
                    disconnected_counter = 0;
                    hal.console->write("isFind1310 false.");
                }
            } else if ( isTen ) {
                if (disconnected_counter < 13 || disconnected_counter == 27) {
                    hal.console->write("|");
                    hal.console->write(disconnected_string[disconnected_counter]);
                    hal.console->write("|");
                    hal.console->write(symbol);
                    hal.console->write("|");
                    if (symbol == disconnected_string[disconnected_counter]) {
                        if (disconnected_counter == 27) {
                            // Строка совпала. Запустить проверку #13#10.
                            isFind1310 = true;
                            hal.console->write(disconnected_string[disconnected_counter]);
                            hal.console->write(".");
                        }
                        disconnected_counter++;
                    } else {
                        hal.uartB->write(13);
                        hal.uartB->write(10);
                        for (int i = 0; i < disconnected_counter; i++) {
                            hal.uartB->write(disconnected_string[i]);
                        }
                        isTen = false;
                        isThirteen = false;
                        disconnected_counter = 0;
                        hal.console->write("isTen false.");
                    }
                } else {
                    disconnected_counter++;
                }      
            } else if ( symbol == 10 && isThirteen == true ) {
                isTen = true;
                hal.console->write("#13#10.");
            } else if ( symbol == 13 ) {
                isThirteen = true;
                hal.console->write("#13.");
            } else {
                if (isThirteen) { 
                    hal.uartB->write(13);
                    isThirteen = false;
                }
                if (isTen) { 
                    hal.uartB->write(10);
                    isTen = false;
                }
                isFind1310Thirteen = false;
                isFind1310 = false;
                disconnected_counter = 0;

                hal.uartB->write(symbol);
            }
        }
    }

}

AP_HAL_MAIN();
