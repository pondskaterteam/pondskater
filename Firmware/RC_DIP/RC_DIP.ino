/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */
 
#include <Servo.h> 
 
// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
const int ledPin = 13;

Servo drive;
Servo servo;

const int driveOut = 6;
const int servoOut = 7;

const int driveIn = 8;
const int servoIn = 9;
const int switchIn = 10;

int durationDrive;
int durationServo;
int durationSwitch;

// the setup routine runs once when you press reset:
void setup() {         
       
	// initialize the digital pin as an output.
	pinMode(ledPin, OUTPUT); 

	drive.attach(driveOut);
	drive.write(0);
	
	servo.attach(servoOut);
	servo.write(90);
	
	pinMode(driveIn, INPUT);
	pinMode(servoIn, INPUT);
	pinMode(switchIn, INPUT);
	
	// Init end
	Blink();
}

// the loop routine runs over and over again forever:
void loop() {
	
	durationSwitch = pulseIn(switchIn, HIGH, 20000);
	
	if (durationSwitch < 600) // ��� �������
	{
		servo.write(90);
		drive.write(0);
		Blink();
		delay(250);
	}
	else
	{
		if (durationSwitch < 1200) // ������ ����������
		{
			digitalWrite(ledPin, HIGH);
			durationDrive = pulseIn(driveIn, HIGH, 20000);
			durationServo = pulseIn(servoIn, HIGH, 20000);
			servo.writeMicroseconds(durationServo);
			drive.writeMicroseconds(durationDrive < 1500 ? durationDrive : 1500);
		}
		else // ����������
		{
			digitalWrite(ledPin, LOW);
			servo.write(45);
			drive.writeMicroseconds(1500);
		}
	}
  
}

void Blink()
{
	digitalWrite(ledPin, HIGH);   // turn the LED on (HIGH is the voltage level)
	delay(250);               // wait for a second
	digitalWrite(ledPin, LOW);    // turn the LED off by making the voltage LOW
}
